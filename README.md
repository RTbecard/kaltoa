# kaltoa: Kalman Filtered Time-of-Arrival Positioning

Kalman filtered  (**kal-**) time-of-arrival positioning (**-toa**) for underwater acoustic telemetry.
Broadly, this package contains mixed model and Kalman filter implementations for time-of-arrival positioning models, geared for high resolution tracking of tagged animals.

This can be viewed as an extension of the features in the [YAPS package](https://github.com/baktoft/yaps).
Utilizing [TMB](https://cran.r-project.org/web/packages/TMB/index.html), YAPS fits state-space models in a mixed model framework where (1) the process component is a Gaussian random walk and (2) the observation component is time-of-arrival positioning error.
The time of tag emissions and the positions of the animal are modeled as latent variables (i\.e\. random effects) to be fitted by `TMB`.

kaltoa does this too, but also provides:

- An extended Kalman filter for fitting telemetry positions, suitable for large data volumes.
- A continuous-time correlated random walk movement model, returning smoother movement paths.
- A mixed detection error distribution that captures large positive outliers resulting from reflected transmissions. 
- A Bayesian HMC model for automatically correcting clock drift in sync-tag equipped receiver arrays. 
- Time-difference-of-arrival positioning and error functions.
- ... and a broad suite of functions for handling and visualizing telemetry data.

The intent of kaltoa is to provide a comprehensive toolset for loading, visualizing, preprocessing, and positioning acoustic telemetry data.
This [user guide](https://rtbecard.gitlab.io/kaltoa/) provides a quick walk through of the main features.
All examples shown in this user guide and in the package documentation are reproducible using the datasets embedded in the package.

To install the package, you can use `devtools`.
If your on windows, you'll need the [latest version of rstan](https://github.com/stan-dev/rstan/wiki/RStan-Getting-Started), although its probably a good idea to do this regardless of your platform.

```r
# Get latest version of rstan
install.packages("rstan", repos = c("https://mc-stan.org/r-packages/", getOption("repos")))
# Install Kaltoa
devtools::install_git(url = "https://gitlab.com/RTbecard/kaltoa.git", build_manual = T)
```

Note: For the current CRAN version of rstan (2.21.1), there is a [bug](https://github.com/stan-dev/rstan/issues/1006) preventing stan code compiling in newer R versions.

You can also download the source package [here](https://gitlab.com/RTbecard/kaltoa/-/jobs/artifacts/main/browse?job=build) to install it locally.

```r
install.packages("./kaltoa_[VERSION].tar.gz", repos = NULL)
```

Lastly, here's some links to the latest compiled package docs for a quick reference:

 - Vignette: [Kalman Filtered Time-of-Arrival Positioning](https://gitlab.com/RTbecard/kaltoa/-/jobs/artifacts/main/raw/doc/kalman_toa.pdf?job=build)
 - Vignette: [Calculating Geometric Positioning Error](https://gitlab.com/RTbecard/kaltoa/-/jobs/artifacts/main/raw/doc/positioning_error.pdf?job=build)
 - [Kaltoa Manual](https://gitlab.com/RTbecard/kaltoa/-/jobs/artifacts/main/raw/kaltoa_manual.pdf?job=build)
