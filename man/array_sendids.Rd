% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/DetectionsList.R
\name{array_sendids}
\alias{array_sendids}
\title{Retrieve sync-tag IDs from DetectionsList object.}
\usage{
array_sendids(detlst)
}
\arguments{
\item{detlst}{A \code{DetectionsList} object}
}
\description{
Retrieve sync-tag IDs from DetectionsList object.
}
\examples{
# ------ Make detections list
# Get detections objects
det_1 <- moerrumsaan_detlst_raw[[1]]
det_2 <- moerrumsaan_detlst_raw[[2]]
det_3 <- moerrumsaan_detlst_raw[[3]]
det_4 <- moerrumsaan_detlst_raw[[4]]
det_5 <- moerrumsaan_detlst_raw[[5]]
det_6 <- moerrumsaan_detlst_raw[[6]]

# Remake DetectionsList
detlst <- DetectionsList(det_1, det_2, det_3, det_4, det_4, det_6)
detlst

# Convert a 'list of Detections' into a DetectionsList object
ldet <- list(det_1, det_2, det_3, det_4, det_4, det_6)
detlst <- do.call(DetectionsList, ldet)
detlst

# Remove double detections from array
detlst <- detlst_apply(detlst, remove_double_detections, dd_thresh = 0.05)

# ------ Viewing data
# Show summary of array detections data
summary(detlst)
# Plot map of array
plot(detlst)
# Return a matrix of array positions
array_positions(detlst)
# Retrieve vector of sync-tag id's
array_sendids(detlst)
}
\seealso{
Other {DetectionsList}: 
\code{\link{DetectionsList}},
\code{\link{array_positions}()},
\code{\link{array_toa}()},
\code{\link{detlst_apply}()},
\code{\link{plot.DetectionsList}()},
\code{\link{subset.DetectionsList}()}
}
\concept{{DetectionsList}}
