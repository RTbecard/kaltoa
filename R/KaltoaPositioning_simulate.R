#' Simulate randomized movement paths from a KaltoaPositioning object
#'
#' Use the covariance matrix from a fitted [KaltoaPositoning] object to sample random states.
#' This is useful for visualizing positioning error in fitted models or propagating state uncertainty into animal movement models via a multiple imputation framework.
#'
#' @param object A fitted [KaltoaPositioning] object.
#'
#'
#' @template seealso
#' @family {post-processing}
#' @example inst/examples/post-processing.R
simulate.KaltoaPositioning <- function(object, nsim = 1){

  require(mvtnorm, quietly = T)
  require(TMB, quietly = T)

  if(object@fit == "Mixed"){

    obj <- object@results$obj
    object@results$obj$report()

    # Get estimated random effect parameters
    par <- obj$env$last.par
    # Retrieve Hessian
    hess <- obj$env$spHess(obj$env$last.par)
    # Index position and velocity values
    is_pos <- names(par) == "position"
    is_vel <- names(par) == "velocity"
    is_ln_sigma_proc <- names(par) == "ln_sigma_proc"
    is_ln_beta_proc <- names(par) == "ln_beta_proc"

    # Check if movement parameters have been fitted
    get_movement <-length( hess[is_ln_sigma_proc, is_ln_sigma_proc]) != 0

    if(get_movement){
      idx_sub <- is_pos | is_vel | is_ln_sigma_proc | is_ln_beta_proc
    }else{
      idx_sub <- is_pos | is_vel
    }

    # Convert hessian to covariance
    Sigma <- solve(hess)[idx_sub, idx_sub]

    lst_out <- list()

    # Generate sim data for movement states
    rdata <- rmvnorm(nsim, mean = par[idx_sub], sigma = Sigma)
    for(i in 1:nsim){

      rpos <- matrix(rdata[i, names(par[idx_sub]) == "position"], ncol = 2, byrow = F)
      lst_out[[i]] <- list(
        type = object@process@type,
        position = rpos)

      if(lst_out[[i]]$type == "CTCRW"){
        rvel <- matrix(rdata[i, names(par[idx_sub]) == "velocity"], ncol = 2, byrow = F)
        rvel <- rbind(0, rvel)
        lst_out[[i]]$velocity = rvel
      }

      # Get movement parameters
      if(get_movement){
        lst_out[[i]]$sigma = exp(rdata[i, names(par[idx_sub]) == "ln_sigma_proc"])
        if(lst_out[[i]]$type == "CTCRW"){
          lst_out[[i]]$beta = exp(rdata[i, names(par[idx_sub]) == "ln_beta_proc"])
        }else{
          lst_out[[i]]$beta = NULL
        }
        lst_out[[i]]$beta = object@process@beta
      }else{
        lst_out[[i]]$sigma = object@process@sigma
        lst_out[[i]]$beta = object@process@beta
      }

      lst_out[[i]]$emission_times <- object@emission_times

      class(lst_out[[i]]) <- c("KaltoaSim", "list")
      attr(lst_out[[i]], "start_time") <- attr(object@toa, "start_time")

    }
    return(lst_out)

  }else if(object@fit  == "Kalman"){

    stop("This feature hasn't been implemented yet.")

  }else{
    stop("Can only simulate from fitted KaltoaPositoning objects.")
  }
}


#' Interpolate movement position
#'
#' An internal Kaltoa function.  Uses a RTS smoother to interpolate state and
#' covariance values for track positions.
#'
#' Returns the expectation and variance of the interpoalted state.
#'
#'
internal_interpolate <- function(
    emis_in, pos_in, vel_in, emission_time, sigma, beta, dims, fF, fQ, is_ctcrw){

  if(emission_time <= emis_in[1]){

    # ------ before start of track
    # Predictions are made by flipping velocity values
    delta = emission_time - emis_in[1]

    state_after <- as.vector(pos_in[1,])
    if(is_ctcrw){state_after <- as.vector(rbind(state_after, vel_in[1,]))}

    if(is_ctcrw){
      # invert velocity values
      idx_vel <- seq(2,dims*2, by = 2)
      state_after[idx_vel] <- -state_after[idx_vel]
    }

    # Get interpolated expected state and variance
    state_before <- fF(beta = beta, delta = -delta, dims = dims) %*% state_after
    sigma_before <- fQ(sigma = sigma, beta = beta, delta = -delta, dims = dims)

    if(is_ctcrw){
      # uninvert velocity values
      idx_vel <- seq(2,dims*2, by = 2)
      state_before[idx_vel] <- -state_before[idx_vel]
    }

    return(list(state = state_before, sigma = sigma_before))

  }else if(emission_time >= emis_in[length(emis_in)]){

    # ------ after end of track
    delta = emission_time - emis_in[length(emis_in)]

    state_before <- as.vector(pos_in[nrow(pos_in),])
    if(is_ctcrw){state_before <- as.vector(rbind(
      state_before, vel_in[nrow(vel_in),]))}

    # Get interpolated expected state and variance
    state_after <- fF(beta = beta, delta = delta, dims = dims) %*% state_before
    sigma_after <- fQ(sigma = sigma, beta = beta, delta = delta, dims = dims)

    return(list(state = state_after, sigma = sigma_after))

  }else{
    # ------ Within track
    idx_after = which(emis_in >= emission_time)[1]
    idx_before = idx_after - 1

    delta_before = emission_time - emis_in[idx_before]
    delta_after =  emission_time - emis_in[idx_after]

    state_before <- as.vector(pos_in[idx_before,])
    if(is_ctcrw){state_before <- as.vector(rbind(state_before, vel_in[idx_before,]))}
    state_after <- as.vector(pos_in[idx_after,])
    if(is_ctcrw){state_after <- as.vector(rbind(state_after, vel_in[idx_after,]))}

    # Get interpolated expected state and variance
    tF_1 <- fF(beta = beta, delta = delta_before, dims = dims)
    tQ_1 <- fQ(sigma = sigma, beta = beta, delta = delta_before, dims = dims)
    state_interp_1 <- tF_1 %*% state_before
    sigma_interp_1 <- tQ_1

    tF_2 <- fF(beta = beta, delta = -delta_after, dims = dims)
    tQ_2 <- fQ(sigma = sigma, beta = beta, delta = -delta_after, dims = dims)
    state_interp_2 <- tF_2 %*% state_interp_1
    sigma_interp_2 <- tF_2 %*% sigma_interp_1 %*% t(tF_2) + tQ_2

    C <- sigma_interp_1 %*% t(tF_2) %*% solve(sigma_interp_2)

    state_interp_3 <- state_interp_1 + C %*% (state_after - state_interp_2)
    sigma_interp_3 <- sigma_interp_1 + C %*% (-sigma_interp_2) %*% t(C)

    return(
      list(
        state = state_interp_3,
        sigma = sigma_interp_3))

  }
}

#' Interpolate simulated movement data
#'
#' Randomly sample interpolated positions from simulated states.
#'
#' @param object A [KaltoaSim] object returned from [simulate.KaltoaPositioning].
#' @param emission_times A vector of emission times to interpolate to.
#' Can be provided as numeric relative detection seconds or absolute POSIXct times.
#' @param extrapolate When TRUE, emission times outside the detection range will be extrapolated.
#' Otherwise, they will be ignored.
#'
#' @template seealso
#' @family {post-processing}
#' @example inst/examples/post-processing.R
interpolate.KaltoaSim <- function(object, emission_times, extrapolate = FALSE){

  require(mvtnorm, quietly = T)

  dims <- ncol(object$position)
  sigma <- object$sigma
  beta <- object$beta
  is_ctcrw <- "velocity" %in% names(object)

  pos_out <- matrix(NA, nrow = length(emission_times), ncol = dims)
  pos_in <- object$position

  emis_in <- object$emission_times

  # Convert POSIX values to relative times
  if("POSIXct" %in% class(emission_times)){
    emission_times <- as.numeric(emission_times) - as.numeric(attr(object, "start_time"))
  }

  # Trim times outside detection range
  if(!extrapolate){
    idx <- emission_times > min(object$emission_times, na.rm = T) & emission_times < max(object$emission_times, na.rm = T)
    emission_times <- emission_times[idx]
  }

  # Define covariance and transfer functions
  if(is_ctcrw){
    # ------ CTCRW model
    fF <- kaltoa:::ou_F
    fQ <- kaltoa:::ou_Q
    vel_in <- object$velocity
    vel_out <- matrix(NA, nrow = length(emission_times), ncol = dims)

  }else{
    # ------ RW model
    fF <- function(beta, delta, dims = 2){
      return(diag(rep.int(x = 1, times = dims)))
    }
    fQ <- function(sigma, beta, delta, dims = 2){
      return(diag(rep.int(x = sigma * sqrt(delta), times = dims)))
    }
  }

  # ------ loop through output times
  for(i in 1:length(emission_times)){

    emission_time = emission_times[i]

    interp <- internal_interpolate(
      emis_in = emis_in, pos_in = pos_in, vel_in = vel_in,
      emission_time = emission_time, sigma = sigma, beta = beta,
      dims = dims, fF = fF, fQ = fQ, is_ctcrw = is_ctcrw)

    # Sample random state value
    rstate <- matrix(rmvnorm(
      n = 1, mean = interp$state, sigma = interp$sigma),
      ncol = dims)

    # Append new values
    emis_in <- c(emis_in, emission_time)
    idx_order <- order(emis_in)
    emis_in <- emis_in[idx_order]

    pos_in <- rbind(pos_in, rstate[1,])[idx_order,]
    if(is_ctcrw){
      vel_in <- rbind(vel_in, rstate[2,])[idx_order,]
    }

    # Save return values
    pos_out[i,] = rstate[1,]
    if(is_ctcrw){
      vel_out[i,] = rstate[2,]
    }

  }

  out = list(
    position = pos_out, emission_times = emission_times,
    posix = attr(object, "start_time") + emission_times)
  if(is_ctcrw){
    out$velocity = vel_out
  }
  out$type = "simulated"

  class(out) <- c("KaltoaInterpolation", "list")
  return(out)
}

#' Interpolate movement data
#'
#' Interpolated positions are estimated from a [KaltoaPositioning] object.
#'
#' @param object A fitted [KaltoaPositioning] object.
#' @param emission_times A vector of emission times to interpolate to.
#' Can be in seconds relative to the TOAMatrix `start_time` or absolute [POSIXct] times.
#' @param extrapolate When TRUE, times outside the detection range will be extrapolated.
#' Otherwise they will be ignored.
#'
#' @details
#' You probably want to interpolate a fitted [KaltoaPositioning] object.
#' For example, to generate positions with regular time intervals that can be further used in a discrete time movement analysis.
#' Additionally, this function will also allow you to interpolate [KaltoaPositioning] objects that have not been fitted.
#' The starting parameters of the model will be interpolated in this case.
#'
#' @template seealso
#' @family {post-processing}
#' @example inst/examples/post-processing.R
interpolate.KaltoaPositioning <- function(object, emission_times, extrapolate = FALSE){

  dims <- ncol(object@hydrophones)
  sigma <- object@process@sigma
  beta <- object@process@beta
  is_ctcrw <- object@process@type == "CTCRW"

  pos_in <- positions(object)
  emis_in <- times(object)

  # Convert POSIX values to relative times
  if("POSIXct" %in% class(emission_times)){
    emission_times <- as.numeric(emission_times) - as.numeric(attr(toa(object), "start_time"))
  }

  # Trim times outside detection range
  if(!extrapolate){
    idx <- emission_times > min(toa(object), na.rm = T) & emission_times < max(toa(object), na.rm = T)
    emission_times <- emission_times[idx]
  }

  pos_out <- matrix(NA, nrow = length(emission_times), ncol = dims)

  # Define covariance and transfer functions
  if(is_ctcrw){
    # ------ CTCRW model
    fF <- kaltoa:::ou_F
    fQ <- kaltoa:::ou_Q
    vel_in <- states(object)[,c(seq(2,dims*2, by = 2))]
    vel_out <- matrix(NA, nrow = length(emission_times), ncol = dims)

  }else{
    # ------ RW model
    fF <- function(beta, delta, dims = 2){
      return(diag(rep.int(x = 1, times = dims)))
    }
    fQ <- function(sigma, beta, delta, dims = 2){
      return(diag(rep.int(x = sigma * sqrt(delta), times = dims)))
    }
  }

  # ------ loop through output times
  for(i in 1:length(emission_times)){

    emission_time = emission_times[i]

    interp <- internal_interpolate(
      emis_in = emis_in, pos_in = pos_in, vel_in = vel_in,
      emission_time = emission_time, sigma = sigma, beta = beta,
      dims = dims, fF = fF, fQ = fQ, is_ctcrw = is_ctcrw)

    state = matrix(interp$state, ncol = dims)

    # # Append new values
    # emis_in <- c(emis_in, emission_time)
    # idx_order <- order(emis_in)
    # emis_in <- emis_in[idx_order]

    # pos_in <- rbind(pos_in, state[1,])[idx_order,]
    # if(is_ctcrw){
    #   vel_in <- rbind(vel_in, state[2,])[idx_order,]
    # }

    # Save return values
    pos_out[i,] = state[1,]
    if(is_ctcrw){
      vel_out[i,] = state[2,]
    }

  }

  out = list(
    position = pos_out, emission_times = emission_times,
    posix = attr(object@toa, "start_time") + emission_times)
  if(is_ctcrw){
    out$velocity = vel_out
  }
  out$type = "fitted"

  class(out) <- c("KaltoaInterpolation", "list")
  return(out)
}
