#' @rdname DetectionsList
#' @name DetectionsList
#' @title Create a DetectionsList object
#' @description
#' This holds all the detections data for a receiver array.
#'
#' @param ... [Detections] objects.
#'
#' @family {DetectionsList}
#'
#' @example inst/examples/DetectionsList.R
DetectionsList <- function(...){

  detlst <- list(...)

  chk <- sapply(detlst, FUN = is, "Detections")

  if(!all(chk)){
    stop("All arguments must be Detections objects.")
  }

  names(detlst) <- sapply(detlst, attr, "send_id")

  class(detlst) <- c("DetectionsList", "list")
  return(detlst)

}

as.DetectionsList <- function(x){
  UseMethod("as.DetectionsList", x)
}

as.DetectionsList.list <- function(x){

  chk_class(x, "list")
  detlst <- x

  chk <- sapply(detlst, FUN = is, "Detections")

  if(!all(chk)){
    stop("All list elements must be Detections objects.")
  }

  names(detlst) <- sapply(detlst, attr, "send_id")

  class(detlst) <- c("DetectionsList", "list")
  return(detlst)

}

#' Plot locations of telemetry receivers.
#'
#' @param detlst A list of `detection` objects.
#' @param ... Additional parameters passed to `plot`.
#'
#' @family {DetectionsList}
#' @template seealso
plot.DetectionsList <- function(detlst, ...){

  df <- data.frame()

  for(i in 1:length(detlst)){
    name <- attr(detlst[[i]], "send_id" )
    x <- attr(detlst[[i]], "x" )
    y <- attr(detlst[[i]], "y" )
    df <- rbind(df, data.frame(name = name, x = x, y = y))
  }

  plot(df$x, df$y, asp = 1, xlab = "x (m)", ylab = "y (m)", ...)
  text(df$x, df$y, labels = df$name, adj = c(0.5, -1))
}

print.DetectionsList <- function(x){

  cat("DetectionsList\n")
  print(summary(x))
}

#' Generate summary statistics for detections list
#'
#' Get the first and last detection times for each tag, in addition to the number of detections in the array and the period in hours spanning all detections.
#'
#' @param detlst A [list] of [Detections] objects.
#'
#' @template seealso
#' @family {DetetionsList}
#' @example inst/examples/DetectionsList.R
summary.DetectionsList <- function(detlst){

  chk_class(detlst, 'DetectionsList')

  df <- data.frame()

  send_ids <- sapply(detlst, attr, "send_id")

  # Remove sync tags
  detlst_filt <- lapply(detlst, function(x){
    x[!x$sync_emission]})

  # Merge all detections into single dataframe
  df <- do.call('rbind.data.frame', detlst_filt)

  # Loop tags and calc summary stats
  tags <- unique(df$detect_id)

  summaries <- lapply(as.list(as.character(tags)),
                      function(x){
                        df_filt <- `[.data.frame`(df, df$detect_id == x,)
                        first <- min(df_filt$second)
                        last <- max(df_filt$second)
                        count <- nrow(df_filt)
                        return(data.frame(tag_id = x,
                                          first = first, last = last,
                                          count = count))
                      })

  out_tags <- do.call('rbind', summaries)
  out_tags$first <- as.POSIXct(out_tags$first, origin = "1970-01-01")
  out_tags$last <- as.POSIXct(out_tags$last, origin = "1970-01-01")
  out_tags$period_h <- as.numeric(out_tags$last - out_tags$first) / 3600

  out_receivers <- data.frame(send_id = sapply(detlst, attr, "send_id"))
  out_receivers$n_emis <- sapply(detlst, function(x){sum(x$sync_emission)})
  out_receivers$n_detectitons <- sapply(detlst, function(x){sum(!x$sync_emission)})
  out_receivers$n_tags <- sapply(detlst, function(x){length(unique(x$detect_id[!x$sync_emission]))})
  out_receivers$det_first <- sapply(detlst, function(x){min(x$second[!x$sync_emission])})
  out_receivers$det_last <- sapply(detlst, function(x){max(x$second[!x$sync_emission])})

  out_receivers$det_first <- as.POSIXct(out_receivers$det_first, origin = "1970-01-01")
  out_receivers$det_last <- as.POSIXct(out_receivers$det_last, origin = "1970-01-01")
  out_receivers$det_period = round(
    as.numeric(
      difftime(out_receivers$det_last, out_receivers$det_first, units = "hours")), 2)

  names(out_receivers) <- c(
    "Send ID", "Emissions", "Detections", "Tags Detected",
    "First Detection", "Last Detection", "Detection Period (h)")

  out <- list(detections = out_tags, receivers = out_receivers)

  class(out) <- c('ArraySummary', "data.frame")
  return(out)

}

print.ArraySummary <- function(x){

  cat("\nReceiver Summary\n")
  prnt <- t(x$receivers[,2:4])
  colnames(prnt) <- x$receivers$`Send ID`
  print(prnt, right = F)

  cat("\nDetections Summary\n")
  df <- x$detections
  df <- df[order(df$first),]
  names(df) <- c("Tag ID", "First detection", "Last detection", "n", "Detection period (h)")
  print(df, row.names = F)
}

#' @rdname array_positions
#' @name array_positions
#' @title Get matrix of receiver positions
#'
#' @param detlst A [DetectionsList] object.
#' @param depth When TRUE, the resulting matrix contains depth.
#'
#' @family {DetectionsList}
#' @example inst/examples/DetectionsList.R
array_positions <- function(detlst, depth = FALSE){

  chk_class(detlst, 'DetectionsList')

  id <- sapply(detlst, attr, "send_id")
  x <- sapply(detlst, attr, "x")
  y <- sapply(detlst, attr, "y")
  z <- sapply(detlst, attr, "depth")

  hydro <- matrix(cbind(x,y), ncol = 2)
  rownames(hydro) = id
  colnames(hydro) = c('x', 'y')

  if(depth){
    if(any(is.na(z))){
      stop("Missing depth values, cannot make 3D array.")
    }
    hydro <- cbind(hydro, z = z)
  }

  # Sort hydrophones
  hydro <- hydro[order(id), ]

  return(hydro)
}


#' @rdname array_positions
#' @usage
#' # Apply an updated matrix of receiver positions to DetectionsList
#' array_positions(x) <- value
`array_positions<-` <- function(x, value){

  chk_class(x, 'DetectionsList')

  if(any(!names(x) %in% rownames(value))){
    stop("Receiver names in x and value do not match.")
  }

  if(any(!c('x', 'y') %in% colnames(value))){
    stop("value must be a matrix with column names 'x' and 'y'.")
  }


  for(name in names(x)){

    attr(x[[name]], 'x') <- value[name, 'x']
    attr(x[[name]], 'y') <- value[name, 'y']
    if('z' %in% colnames(value)){
      attr(x[[name]], 'depth') <- value[name, 'z']
    }

  }

  return(x)
}

#' Get time-of-arrival matrix from receiver array
#'
#' For a given tag ID, this will return a time-of-arrival matrix that can be used for positioning.
#'
#' @param detlst A [DetectionsList] object
#' @param tag_id A character string holding the tag ID to return arrival times for.
#' @param group_thresh Threshold in seconds for grouping sync-tag detection-emission pairs.
#' @param quietly When `TRUE`, no messages will be printed.
#'
#' @return A [TOAMatrix] object.
#' @family {DetectionsList}
#' @example inst/examples/DetectionsList.R
array_toa <- function(detlst, tag_id, group_thresh = 0.5, quietly = F){

  chk_class(detlst, 'DetectionsList')

  # Get tag_id
  tag_id <- as.character(tag_id)

  # Name list items by send_id
  names(detlst) <- sapply(detlst, attr, "send_id")

  if(tag_id %in% names(detlst) & !quietly){
    message("Selected tag is a sync tag.  Self-detections by the emitting receiver will be ignored.")
  }

  # ------ Make dataframe of all tag detections
  tmp <- lapply(detlst, function(x){
    out = x[x$detect_id == tag_id, c("second", "millisecond")]
    if (nrow(out) == 0 || tag_id == as.character(attr(x, 'send_id'))){
      # Return single NA is no detections or if receiver is emitting receiver
      return (data.frame())
    }else{
      out$send_id = attr(x, "send_id")
      row.names(out) <- NULL
      return(as.data.frame(out))
    }})
  df <- do.call('rbind.data.frame', tmp)
  if(nrow(df) == 0){stop("No detections found for tag: ", tag_id)}
  df <- df[order(df$second, df$millisecond),]
  rownames(df) <- NULL

  if(all(is.na(df$second)) & !quietly){
    message("No detections within time period.  Returning NULL.")
    return(NULL)
  }

  # Normalize times
  first_det <- min(df$second,na.rm = T)
  df$time <- df$second - first_det + df$millisecond*1e-3
  df <- df[, c("send_id", "time")]

  # ------ Mark transmission instances
  df$delta <- c(0, diff(df$time))
  df$instance <- 0
  idx <- which(df$delta > group_thresh)
  df$instance <- cumsum(as.integer(df$delta > group_thresh)) + 1

  # ------ Convert to wide data.frames
  # First detection should be taken by reshape
  suppressWarnings(
    df_wide <- reshape(df[,-3], v.names = "time", timevar = "send_id",
                       idvar = "instance", direction = 'wide'))

  # ------ Add missing missing columns with NAs
  colnames <- paste("time", names(detlst), sep = '.')
  idx.missing <- which(sapply(as.list(colnames),
                              function(x){!x %in% names(df_wide)}))
  dummy <- matrix(NA, nrow = nrow(df_wide), ncol = length(idx.missing),
                  dimnames = list(NULL, colnames[idx.missing]))
  df_wide <- cbind(df_wide, dummy)

  # ------ Order columns alphabetically
  idx.cols <- 2:ncol(df_wide)
  names.cols <- names(df_wide)[idx.cols]
  df_wide <- df_wide[c(1, 1+ order(names.cols))]

  # ------ Fill toa matrix
  det_mat <- as.matrix(df_wide[,2:ncol(df_wide)])
  # order columns alphabetically
  det_mat <- det_mat[,order(colnames(det_mat)), drop = F]
  toa <- matrix(NaN, nrow = max(df_wide$instance), ncol = ncol(det_mat))
  toa[df_wide$instance,] <- det_mat

  colnames(toa) <- sort(names(detlst))

  attr(toa, "start_time") <- as.POSIXct(first_det, origin = "1970-01-01")
  attr(toa, "send_id") <- tag_id

  class(toa) <- c("TOAMatrix", class(toa))

  return(toa)
}


#' Retrieve sync-tag IDs from DetectionsList object.
#'
#' @param detlst A `DetectionsList` object
#'
#' @family {DetectionsList}
#' @example inst/examples/DetectionsList.R
array_sendids <- function(detlst){

  chk_class(detlst, 'DetectionsList')

  out <- unlist(lapply(detlst, attr, "send_id"))
  names(out) <- NULL

  return(out)

}

#' Subset a DetectionsList object
#'
#' The subset expression is passed onto the function [subset.Detections] for each item in the list.
#'
#' @param x A [DetectionsList] object.
#' @param ... Arguments passed to [subset.Detections].
#'
#' @family {DetectionsList}
#' @example inst/examples/DetectionsList.R
subset.DetectionsList <- function(x, ...){

  out <- detlst_apply(x, subset, ...)
  return(out)

}


#' lapply wrapper for DetectionsList objects
#'
#' This will apply the [lapply] function, but return a [DetectionsList] object.
#'
#' @param X A [DetectionsList] object.
#' @param FUN Function to apply to all `DetectionsList` objects.
#' @param ... Arguments passed to `FUN`.
#'
#' @family {DetectionsList}
#' @example inst/examples/DetectionsList.R
detlst_apply <- function(X, FUN, ...){

  chk_class(X, "DetectionsList")

  lst <- lapply(X, FUN, ...)

  return(do.call("DetectionsList", lst))

}
