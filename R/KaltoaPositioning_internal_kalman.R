



#' Wrap the Kalman filter covariance vector into a 3 dimensional array.
#'
#' `cpp_kalman` returns the state covariance estimates as long vectors.
#' This is a conveniance function which will reshape those vectors back into 3 dimensional arrays.
#'
#' @param kalman A `KaltoaKalman` object returned by the function `cpp_kalman()`.
#'
kaltoa_wrap_P <- function(kalman){

  if(class(kalman) != "KaltoaKalman"){
    stop("Argument `kalman` must be an object of class 'KaltoaKalman' returned by the function `cpp_kalman()`.")
  }

  n = nrow(kalman$x_prior)
  n_states = ncol(kalman$x_prior)

  P_prior <- array(NA, dim = c(n_states, n_states, n))
  P_post <- array(NA, dim = c(n_states, n_states, n))

  for(i in 1:n){
    for(col in 1:n_states){
      for(row in 1:n_states){
        j = (i-1)*n_states*n_states + (col-1)*n_states + row;
        P_prior[row, col, i] = kalman$P_prior[j]
        P_post[row, col, i] = kalman$P_post[j]
      }
    }
  }

  kalman$P_prior = P_prior
  kalman$P_post = P_post

  return(kalman)
}

#' Apply Rauch–Tung–Striebel Smoother to Kalman Results
#'
#' The Rauch–Tung–Striebel smoother effectively updates the state estimates of a Kalman filter by reapplying the filter in the reverse direction.
#' This can result in more accurate state (i.e. position) estimates, as each state prediction is updated with future information.
#'
#' This function is used internally by `kaltoa` to smooth the results of Kalman filters used in the positioning models.
#'
#' @param kalman A `KaltoaKalman` object returned by the function `cpp_kalman()`.
#'
kaltoa_smooth <- function(kalman){

  if(class(kalman) != "KaltoaKalman"){
    stop("Argument `kalman` must be an object of class 'KaltoaKalman' returned by the function `cpp_kalman()`.")
  }

  # Wrap covariance matrices
  if(class(kalman$P_prior) != 'array'){
    kalman = kaltoa_wrap_P(kalman)
  }

  n = nrow(kalman$x_prior)
  n_states = ncol(kalman$x_prior)
  n_dims = if(kalman$process_model == "CTCRW"){(n_states-1)/2}else{n_states-1}

  x_smooth = matrix(NA, nrow = n, ncol = n_states)
  P_smooth = array(NA, dim = c(n_states, n_states, n))

  x_smooth[n,] = kalman$x_post[n,]
  x_smooth[1,] = kalman$x_post[1,]

  P_smooth[,,n] = kalman$P_post[,,n]
  P_smooth[,,1] = kalman$P_post[,,1]

  # make stationary process model (animal does not move)
  mF = diag(1, nrow = n_states, ncol = n_states)
  mF[n_states, n_states] = 0

  for(i in (n-1):2){
    if (kalman$process_model == "CTCRW"){
      delta <- kalman$transmission_times[i+1] - kalman$transmission_times[i]
      mF[1:(n_states-1), 1:(n_states-1)] =
        ou_F(beta = kalman$beta_pro, delta = delta, dims = n_dims)
    }
    C <- kalman$P_post[,,i] %*% t(mF) %*% solve(kalman$P_prior[,,i+1])

    x_smooth[i,] <- kalman$x_post[i,] + C %*% (x_smooth[i+1,] - kalman$x_prior[i+1, ])
    P_smooth[,,i] <- kalman$P_post[,,i] + C %*% (P_smooth[,,i+1] - kalman$P_prior[,,i+1]) %*% t(C)
  }

  kalman$x_smooth <- x_smooth
  kalman$P_smooth <- P_smooth

  return(kalman)

}


