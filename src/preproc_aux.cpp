#include <Rcpp.h>
using namespace Rcpp;


// A binary search for ordered detection dataframe.  Will return the index of
// the closest matching detection time.
// [[Rcpp::export]]
int cpp_binarySearchDetections(IntegerVector &det_s, NumericVector &det_ms, double val) {

  // Init indexes
  int start = 1;
  int end = det_s.length() - 1;
  int idx;
  double idx_val;

  while ( (end-start) > 1){
    idx = floor((start + end) / 2);
    idx_val = det_s[idx] + det_ms[idx]*1e-3;

    if (val > idx_val) {
      start = idx;
    }else{
      end = idx;
    }
    // std::printf("start %i end %i idx %i idx_val %.4f \n", start, end, idx, idx_val);
  }

  double start_val;
  double end_val;

  if (start == end){
    return start + 1;
  }else{
    start_val = det_s[start] + det_ms[start]*1e-3;
    end_val = det_s[end] + det_ms[end]*1e-3;

    if (std::abs(start_val - val) < std::abs(end_val - val)){
      return start + 1;
    }else{
      return end + 1;
    }
  }
}

// For filtered, sorted detection dataframes, return all traversal times.
// NumericVector cpp_transmissionLatency(DataFrame det_rcv_filt, DataFrame det_snd_filt, double max_lat, bool add_drift = true) {
//
//   double sig_rcv;
//   double sig_snd;
//   double offset;
//   double t;
//
//   int idx_snd;
//
//   IntegerVector det_rcv_s = det_rcv_filt["second"];
//   NumericVector det_rcv_ms = det_rcv_filt["millisecond"];
//
//   IntegerVector det_snd_s = det_snd_filt["second"];
//   NumericVector det_snd_ms = det_snd_filt["millisecond"];
//
//   // Transmission times
//   int n = det_rcv_filt.nrows();
//   NumericVector trav (n);
//
//   // Loop received times
//   offset = 0;
//   for(int idx_rcv = 0; idx_rcv < n; idx_rcv ++){
//     // get signal receiving time
//     sig_rcv = det_rcv_s[idx_rcv] + det_rcv_ms[idx_rcv]*1e-3;
//
//     // get signal sending time
//     idx_snd = cpp_binarySearchDetections(det_snd_s, det_snd_ms, sig_rcv - offset) - 1;
//     sig_snd = det_snd_s[idx_snd] + det_snd_ms[idx_snd]*1e-3;
//
//     // Save traversal time
//     t = sig_rcv - sig_snd;
//     if(std::abs(t - offset) < max_lat || idx_rcv == 0){
//       trav[idx_rcv] = t;
//       // Add clock drift
//       if(add_drift){
//         offset = t;
//       }
//     }else{
//       trav[idx_rcv] = R_NaN;
//     }
//
//     // std::printf("det_rcv_s[idx_rcv] %i, det_rcv_ms[idx_rcv] %f; det_snd_s[idx_snd] %i, det_snd_ms[idx_snd] %f; t %.3f, idx_snd %i \n",
//     //             det_rcv_s[idx_rcv], det_rcv_ms[idx_rcv], det_snd_s[idx_snd], det_snd_ms[idx_snd], t, idx_snd);
//
//   }
//   return trav;
// }

// For filtered, sorted detection dataframes, return all traversal times.
// [[Rcpp::export]]
NumericVector cpp_transmissionLatency(DataFrame det_rcv_filt, DataFrame det_snd_filt, double group_thresh) {

  double sig_rcv;
  double sig_snd;

  int idx_snd = 0;

  IntegerVector det_rcv_s = det_rcv_filt["second"];
  NumericVector det_rcv_ms = det_rcv_filt["millisecond"];

  IntegerVector det_snd_s = det_snd_filt["second"];
  NumericVector det_snd_ms = det_snd_filt["millisecond"];

  // Transmission times
  int n_rcv = det_rcv_filt.nrows();
  int n_snd = det_snd_filt.nrows();
  NumericVector latency(n_rcv);

  int start_time = det_snd_s[0];

  // set first emission time
  sig_snd = det_snd_s[0]  - start_time + det_snd_ms[0]*1e-3;

  double thresh;

  // Loop received times
  for(int idx_rcv = 0; idx_rcv < n_rcv; idx_rcv ++){
    // get signal receiving time
    sig_rcv = det_rcv_s[idx_rcv] - start_time + det_rcv_ms[idx_rcv]*1e-3;

    // Find next sync tag emission within range
    thresh = sig_rcv - group_thresh;
    while(sig_snd < thresh && idx_snd < n_snd){
      idx_snd ++;
      sig_snd = det_snd_s[idx_snd]  - start_time + det_snd_ms[idx_snd]*1e-3;
    }

    // Save traversal time
    if(sig_snd < (sig_rcv + group_thresh) && idx_snd < n_snd){
      latency[idx_rcv] = sig_rcv - sig_snd;
    }else{
      latency[idx_rcv] = R_NaN;
    }

    // std::printf("det_rcv_s[idx_rcv] %i, det_rcv_ms[idx_rcv] %f; det_snd_s[idx_snd] %i, det_snd_ms[idx_snd] %f; t %.3f, idx_snd %i \n",
    //             det_rcv_s[idx_rcv], det_rcv_ms[idx_rcv], det_snd_s[idx_snd], det_snd_ms[idx_snd], t, idx_snd);

  }
  return latency;
}

// Count the number of detections per bin for a detection dataframe.
// [[Rcpp::export]]
NumericVector cpp_binnedDetections(DataFrame det_rcv_filt, int start, int stop, double bin_period){

  // Get columns from dataframe
  IntegerVector det_s = det_rcv_filt["second"];
  NumericVector det_ms = det_rcv_filt["millisecond"];

  // number of bins to count in
  int n = floor((stop-start) / bin_period);
  // init output vector
  NumericVector count (n);

  int idx_start;
  int idx_end;
  double val;

  // loop and count bins
  for(int i = 0; i < n; i++){
    val = i*bin_period + start;
    idx_start = cpp_binarySearchDetections(det_s, det_ms, val);
    val = (i+1)*bin_period + start;
    idx_end = cpp_binarySearchDetections(det_s, det_ms, val);
    count[i] = idx_end - idx_start;
  }
  return(count);
}


// [[Rcpp::export]]
DataFrame cpp_driftInterpolation(DataFrame det, DataFrame lat){

  IntegerVector det_s = det["second"];
  NumericVector det_ms = det["millisecond"];

  IntegerVector lat_s = lat["second"];
  NumericVector lat_ms = lat["millisecond"];
  NumericVector lat_l = lat["latency"];

  int n_l = lat.nrows();
  int n_while = n_l - 2;
  int n_d = det.nrows();

  double det_time;
  double lat_time_1;
  double lat_time_2;
  double lat_lat_1;
  double lat_lat_2;

  double tmp1;
  double tmp2;
  double intrp;

  // loop detections
  int i_l = 0;
  for(int i_d = 0; i_d < n_d; i_d++){

    det_time = det_s[i_d] + det_ms[i_d]*1e-3;

    // Increment latency index until it reaches detection time
    while(i_l <= n_while){
      // Get time values
      lat_time_1 = lat_s[i_l] + lat_ms[i_l]*1e-3;
      lat_time_2 = lat_s[i_l + 1] + lat_ms[i_l + 1]*1e-3;

      if(lat_time_2 > det_time){
        break;
      }else{
        i_l += 1;
      }
    }

    if(det_time < lat_time_1){
      // Value before interpolation range: use first value
      intrp = lat_l[0];
      // intrp = R_NaN;
    }else if(det_time > lat_time_2){
      // Value after interpolation range: use last value
      intrp = lat_l[n_l-1];
      // intrp = R_NaN;
    }else{
      // Value within interpolation range: linear interpolation
      lat_lat_1 = lat_l[i_l];
      lat_lat_2 = lat_l[i_l + 1];

      tmp1 = lat_lat_2 - lat_lat_1;
      tmp2 = (det_time - lat_time_1) / (lat_time_2 - lat_time_1);
      intrp = lat_lat_1 + tmp1*tmp2;
    }

    // std::printf("lat_time_1 %.1f det_time %.4f lat_time_2 %.1f    lat_l[i] %.4f intrp %.4f lat_l[i+1] %.4f \n",
    //             lat_time_1, det_time, lat_time_2, lat_l[i_l], intrp, lat_l[i_l + 1]);

    // Update dataframe times
    tmp1 = det_time - intrp;
    tmp2 = floor(tmp1);
    det_ms[i_d] = (tmp1-tmp2)*1e3;
    // std::printf("det_s[i_d] %i, tmp1 %f", det_s[i_d], tmp1);
    det_s[i_d] = (int) tmp2;
    // std::printf(", det_s[i_d] %i det_ms[i] %.1f \n", det_s[i_d], det_ms[i_d]);
  }

  return det;
}

// ------------------------- Linear interpolation ------------------------------

// [[Rcpp::export]]
NumericVector cpp_linear_interpolation(NumericVector x_in, NumericVector y_in, NumericVector x_out){

  // Init indicies
  int in_i = 0;
  int in_n = x_in.size();

  int out_n = x_out.size();
  // init output vector
  NumericVector y_out(out_n);

  double scale;

  // Loop output values
  for(int out_i = 0; out_i < out_n; out_i++){

    // Find next matching output index
    while(x_in(in_i) < x_out(out_i) && in_i < (in_n - 1)){
      in_i ++;
    }

    if(x_out(out_i) <= x_in(0)){
      // out_i is at beginning
      y_out(out_i) = y_in(0);
    }else if(x_out(out_i) >= x_in(in_n-1)){
      // out_i is at end
      y_out(out_i) = y_in(in_n-1);
    }else{
      // interpolate
      scale = (x_out(out_i) - x_in(in_i - 1)) / (x_in(in_i) - x_in(in_i - 1));
      y_out(out_i) = y_in(in_i - 1) + (y_in(in_i) - y_in(in_i - 1))*scale;
    }

    // Rcpp::Rcout << "x_in: " << x_in <<
    //   ", in_i: " << in_i <<
    //   ", out_i: " << out_i <<
    //   ", x_out(out_i): " << x_out(out_i) <<
    //   ", y_out(out_i): " << y_out(out_i) <<
    //   ", x_in(in_i): " << x_in(in_i-1) <<
    //   ", x_in(in_i): " << x_in(in_i) <<
    //   ", scale: " << scale << "\n";
  }
  return(y_out);
}

// Count the mean latency per bin for latency data.frame.
// [[Rcpp::export]]
DataFrame cpp_binnedLatency(DataFrame lat, double bin_period){

  // Get columns from dataframe
  IntegerVector lat_s = lat["second"];
  NumericVector lat_ms = lat["millisecond"];
  NumericVector lat_l = lat["latency"];

  int start = lat_s[0];
  int stop = lat_s[lat.nrows() - 1];

  // number of bins to count in
  int n = floor((stop-start) / bin_period);
  // init output vector

  IntegerVector out_s (n);
  NumericVector out_ms (n), out_l (n);

  int idx_start, idx_end;
  double val;
  double tmp_s, tmp_ms, tmp_l;

  // loop and count bins
  for(int i = 0; i < n; i++){
    val = i*bin_period + start;
    idx_start = cpp_binarySearchDetections(lat_s, lat_ms, val);
    val = (i+1)*bin_period + start;
    idx_end = cpp_binarySearchDetections(lat_s, lat_ms, val);
    if(idx_start == idx_end){
      out_s[i] = out_ms[i] = out_l[i] = R_NaN;
    }else{
      tmp_s = tmp_ms = tmp_l = 0;
      for(int i2 = idx_start; i2<= idx_end; i2++){
        tmp_s += lat_s[i2];
        tmp_ms += lat_ms[i2];
        tmp_l += lat_l[i2];
      }
      tmp_s /= idx_end - idx_start + 1;
      tmp_ms /= idx_end - idx_start + 1;
      tmp_l /= idx_end - idx_start + 1;

      out_s[i] = tmp_s;
      out_ms[i] = tmp_ms;
      out_l[i] = tmp_l;
    }
  }

  DataFrame out = DataFrame::create(Named("second") = out_s,
                           Named("millisecond") = out_ms,
                           Named("latency") = out_l);

  return(out);

}


// ===================== Helper log scale functions ============================
double logSumExp(double x, double y){
  double m = std::max(x, y);
  double out = m + log(exp(x - m) + exp(y - m));
  return(out);
}

double logSumExp(NumericVector x){
  double m = max(x);
  double out = m + log(sum(exp(x - m)));
  return(out);
}

NumericVector logSumExp(NumericVector x, NumericVector y){

  int n = x.length();
  NumericVector out(n);

  for(int i = 0; i < n; i++){
    out[i] = logSumExp(x[i], y[i]);
  }
  return(out);
}

// ======================= HMM reflection detection ============================
namespace hmm_reflections{

  // Update vector X with new emission log probabilities
  void update_emission(NumericVector &x, double l, double refl_lat, double sd){

    x[0] = x[1] = R::dnorm(l, 0.0, sd, true);
    x[2] = R::dnorm(l + refl_lat, 0, sd, true);
    x[3] = R::dnorm(l - refl_lat, 0, sd, true);
  }

  // Update matrix with transition log probabilities
  void update_transition(NumericMatrix &X){
    double x = log(0.5);
    double y = R_NegInf;
    X(0,_) = NumericVector::create(x, y, x, y);
    X(1,_) = NumericVector::create(y, x, y, x);
    X(2,_) = NumericVector::create(y, x, y, x);
    X(3,_) = NumericVector::create(x, y, x, y);
  }

};


// Calculate the log likelihood of the reflection HMM model
// [[Rcpp::export]]
double cpp_hmm_reflections(NumericVector d_lat, double refl_lat, double sd){
  // All probabilities are in log scale

  int n_states = 4;
  int n_obs = d_lat.size();
  double tmp;

  // Set initial state probabilities
  // Y, N, YN, NY
  NumericVector alpha_p = NumericVector::create(0.5, 0.5, 0, 0);
  alpha_p = log(alpha_p);
  NumericVector alpha = NumericVector(n_states);

  // Make state transition matrix
  NumericMatrix transition(n_states, n_states);
  hmm_reflections::update_transition(transition);

  // Make empty emissions vector
  NumericVector emission(n_states);

  // Start forward algorithm
  double ll = 0;
  NumericVector state_prob(n_states);
  for(int t = 0; t < n_obs; t++){
    // Update emission matrix
    hmm_reflections::update_emission(emission, d_lat[t], refl_lat, sd);

    // Rcout << "\n ------ t: " << t << "\n";
    // Rcout << "alpha_p: " << alpha_p << "\n";
    // Rcout << "transition: \n" << transition << "\n";

    // combine transition and state probabilities
    for(int k = 0; k < n_states; k++){
      if (k == 0){
        state_prob = transition(k,_) + alpha_p(k);
      }else{
        state_prob = logSumExp(state_prob, transition(k,_) + alpha_p(k));
      }
      // Rcout << "state_prob: " << state_prob << "\n";
    }

    // Add emission probability
    alpha = emission + state_prob;

    // Rcout << "\nemission: " << emission << "\n";
    // Rcout << "alpha: " << alpha << "\n";

    // increment previous alpha values
    alpha_p = alpha;
  }

  // Sum final joint probabilities for each state
  ll = logSumExp(alpha);

  // Rcout << "ll: " << ll << "\n\n";


  return ll;
}


// [[Rcpp::export]]
NumericVector cpp_hmm_reflections_viterbi(NumericVector d_lat, double refl_lat, double sd){
  // algorithm taken from wikipedia
  // https://en.wikipedia.org/wiki/Viterbi_algorithm

  int k = 4;            // number of states
  int t = d_lat.size(); // number observations

  // Initial State probabilities
  NumericVector alpha = NumericVector::create(0.1, 0.9, 0, 0);
  alpha = log(alpha);

  // Make state transition matrix
  NumericMatrix A(k, k);
  hmm_reflections::update_transition(A);

  // Make empty emissions vector
  NumericVector B(k);

  // Viterbi vectors
  NumericMatrix T1(k,t);  // prob of most likely path
  NumericMatrix T2(k,t);  // most likely previous state

  // Rcout << "debug a\n";

  // ------ Init values for first time step
  // update emissions matrix
  hmm_reflections::update_emission(B, d_lat[0], refl_lat, sd);
  for(int i = 0; i < k; i++){
    T1(i,0) = alpha[i] + B[i];
    T2(i,0) = 0;  // This value is never read
  }

  // Rcout << "debug b\n";

  // Recurse through remaining observations
  NumericVector p(k);
  for(int j = 1; j < t; j++){
    hmm_reflections::update_emission(B, d_lat[j], refl_lat, sd);
    for(int i = 0; i < k; i++){
      // Rcout << "j, i: " << j << ", " << i << "\n";

      // Probabilities to state i
      p = T1(_, j-1) + A(_, i) + B[i];
      T1(i, j) = max(p);
      T2(i, j) = which_max(p);
    }
  }

  // Rcout << "debug c\n";

  // init output vector holding likely states
  NumericVector x(t);
  int i_max;
  i_max = which_max(T1(_,t-1));
  x[t-1] = i_max;

  // Travel backwards and map likely path
  for(int j = t - 1; j > 0; j--){
    x[j-1] = T2(x[j], j);
  }

  // Rcout << "debug d\n";

  return(x);
}
