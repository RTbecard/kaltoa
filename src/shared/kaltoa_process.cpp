#include "kaltoa_process.h"

/*******************************************************************************
 *  Functions for implementing OU velocity process in Kalman filter.
 *  See vignette for details.
 *
 *  Johnson, D. S., London, J. M., Lea, M. A., & Durban, J. W. (2008).
 *    Continuous‐time correlated random walk model for animal telemetry data.
 *    Ecology, 89(5), 1208-1215.
 ******************************************************************************/

//==============================================================================
// ================== continuous time correlated random walk ===================

// Position estimate variance
template<class T>
T CTCRW::ou_var_mu(const T &sigma, const T &beta, const T &delta){
  T a, b, c, r;
  a = pow(sigma, 2)/pow(beta,2);
  b = (2/beta)*(1 - exp(-beta*delta));
  c = (1/(2*beta))*(1 - exp(-2*beta*delta));
  r = a * (delta - b + c );
  return r;
}

// Velocity estimate covariance
template<class T>
T CTCRW::ou_var_v(const T &sigma, const T &beta, const T &delta){
  T a, b, r;
  a = pow(sigma,2);
  b = -2*beta*delta;
  r =  a * (1 - exp(b)) / (2*beta);
  return r;
}

// position/velocity estimate covariance
template<class T>
T CTCRW::ou_cov(const T &sigma, const T &beta, const T &delta){
  T a, b;
  a = pow(sigma, 2)/(2*pow(beta,2));
  b = 1 - 2*exp(-beta*delta) + exp(-2*beta*delta);
  return  a * b;
  }

/************************** Kalman Filter functions ***************************/
// Populate 4x4 covariance matrix "mat"
template<class T>
void CTCRW::f_Q(matrix<T> &mat, const T &sigma, const T &beta, const T &delta, const int dims){
  // Calculate covariance values
  T V_mu, V_v, C;
  V_mu = ou_var_mu(sigma, beta, delta);
  V_v = ou_var_v(sigma, beta, delta);
  C = ou_cov(sigma, beta, delta);
  // Update covariance matrix
  for (int i = 0; i < dims; i++){
    int offset = i*2;
    mat(offset, offset) = V_mu;
    mat(offset + 1, offset + 1) = V_v;
    mat(offset, offset + 1) = C;
    mat(offset + 1, offset) = C;
  }
}

// Populate 4x4 Transformation Matrix "mat"
template<class T>
void CTCRW::f_F(matrix<T> &mat, const T &beta, const T &delta, const int dims){
  // Calculate elements
  T f_mu_2, f_v_2;
  f_mu_2 = (T(1) - exp(-beta*delta))/beta;
  f_v_2 = exp(-beta*delta);
  // Update transformation matrix
  for (int i = 0; i < dims; i++){
    int offset = i*2;
    mat(offset, offset) = 1;
    mat(offset, offset + 1) = f_mu_2;
    mat(offset + 1, offset + 1) = f_v_2;
  }
}

//==============================================================================
// ============================= random walk ===================================
// Populate dims x dims covariance matrix "mat"
template<class T>
void RW::f_Q(matrix<T> &mat, const T &sigma, const T &delta, const int dims){
  // Variance sigma^2 for Random walk scales linearly with time step (delta)
  T V = delta*pow(sigma, 2);
  for (int i = 0; i < dims; i++){
    mat(i, i) = V;
  }
}

