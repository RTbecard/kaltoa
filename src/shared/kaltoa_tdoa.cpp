#include "kaltoa_tdoa.h"

// See: Smith, J. O., Abel, J. S. 1987. Closed-Form Least-Squares Source Location
// Estimation from Range-Difference Measurements.  IEE Transactions on acoustic,
//  speech, and signal processing.  35(12):1661-1669.
Eigen::MatrixXd TDOA::smith_abel(
    const Eigen::MatrixXd &toa,
    const Eigen::MatrixXd &pos_hydro,
    const double &c,
    const Eigen::VectorXd &W){

  using Eigen::MatrixXd;
  using Eigen::VectorXd;

  // Check for valid input
  if (toa.cols() != pos_hydro.rows()){
    throw std::invalid_argument("toa must have number of columns equal to rows in pos_hydro.");
  }
  if (W.size() != pos_hydro.rows()){
    throw std::invalid_argument("W must have size equal to pos_hydro.");
  }

  int n_samples, n_hydro, n_delta, n_dims, na_count;

  VectorXd pos_hydro_offset, toa_sub;
  MatrixXd pos_hydro_sub, S, R, P, I, P_ortho, W_mat, pos, S_w, d, W_sub, delta, X, X_inv;
  double  R_s, R_s_plus, R_s_min, a, b, C;

  VectorXi na_check(toa.cols());

  n_samples= toa.rows();
  n_dims= pos_hydro.cols();

  // init return matrix
  MatrixXd ret(n_samples, n_dims);

  #ifdef PKGDEBUG
  Rcpp::Rcout << "------ TOA positioning\n";
  Rcpp::Rcout << "--- W:\n" << W << "\n";
  Rcpp::Rcout << "--- pos_hydro:\n" << pos_hydro << "\n";
  #endif

  for(int i = 0; i < n_samples; i++){

    /***************************************************************************
     * Subset current toa row
     * - Remove NA values, save vector of resulting data to process
     **************************************************************************/

    // Count non-NA hydrophones
    for(int j = 0; j < pos_hydro.rows(); j++){
      na_check(j) = (int) !R_IsNA(toa(i, j));
    }
    n_hydro = na_check.sum();
    n_delta = n_hydro - 1;

    // Skip if too few hydrophones
    if (n_hydro < (n_dims + 1)){
      ret.row(i).fill(NA_REAL);
      continue;
    }

    // ------fill hydrophone and toa subsets
    // Remove NA values from dataset
    pos_hydro_sub = MatrixXd(n_hydro, n_dims);
    toa_sub = VectorXd(n_hydro);
    W_sub = VectorXd(n_hydro);

    na_count = 0;
    for(int j = 0; j < pos_hydro.rows(); j++){
      if(!R_IsNA(toa(i, j))){
        pos_hydro_sub.row(na_count) = pos_hydro.row(j);
        toa_sub(na_count) = toa(i, j);
        W_sub(na_count) = W(j);
        na_count += 1;
      }
    }

    /***************************************************************************
     * Prepare matricies for positioning
     **************************************************************************/

    // ------ Sort hydrophones
    // Find lowest weight
    int idx_W = 0; double W_highest =  W_sub(0);
    for(int j = 1; j < n_hydro; j++){
      if(W_sub(j) > W_sub(idx_W)){
        idx_W = j; W_highest = W_sub(idx_W);
      }
    }
    // move lowest weighted hydrophone to row 0 (this will be the reference)
    VectorXd pos_hydro_tmp= VectorXd(n_dims);
    double toa_tmp;
    double W_tmp;
    if(idx_W != 0){
      pos_hydro_tmp = pos_hydro_sub.row(0);
      toa_tmp = toa_sub(0);
      W_tmp = W_sub(0);

      pos_hydro_sub.row(0) = pos_hydro_sub.row(idx_W);
      toa_sub(0) = toa_sub(idx_W);
      W_sub(0) = W_sub(idx_W);

      pos_hydro_sub.row(idx_W) = pos_hydro_tmp;
      toa_sub(idx_W) = toa_tmp;
      W_sub(idx_W) = W_tmp;
    }

    // Normalize hydrophones, so reference is at position 0,0,0
    // Output remaining hydrophones in matrix S
    pos_hydro_offset = (VectorXd) pos_hydro_sub.row(0);
    S = MatrixXd(n_delta, n_dims);
    for(int j = 1; j < n_hydro; j ++){
      S.row(j-1) = (VectorXd) pos_hydro_sub.row(j) - pos_hydro_offset;
    }

    // Define d: Range distance differences between all sensors
    // TOA differences are proportional to range differences, convert
    // TOA diffs (s) to range diffs (m).
    // Create matrix of distances between sensors i,j
    d = MatrixXd(n_delta, 1);
    for(int j = 1; j < n_hydro; j++){
      d(j-1, 0) = (toa_sub(j) - toa_sub(0)) * c;
    }

    // columns in d represent distance differences between hydro and reference
    // hydro

    // Define R: Distances between Reference sensor (id: 1) and other sensors
    R = MatrixXd(n_delta, 1);
    for(int j = 0; j < n_delta; j ++){
      R(j,0) = sqrt( pow( S.row(j).array(), 2 ).sum() );
    }

    // delta: R - d
    delta = MatrixXd(n_delta, 1);
    delta = pow(R.array(), 2) - pow(d.array(),2);

    // Weighting matrix.
    W_mat = MatrixXd(n_delta, n_delta); W_mat.fill(0);
    for(int j = 0; j < n_delta; j++){
      W_mat(j, j) = W_sub(j+1);
    }

    if(n_hydro > (n_dims + 1)){
      /*************************************************************************
       * Use generalized form of Spherical Interpolation
       ************************************************************************/

      // Identity matrix
      I = MatrixXd::Identity(n_delta, n_delta);

      // Orthogonal compliment of P, with respect to d
      P_ortho =  I - ( (d * d.transpose() ) / (d.transpose() * d).value() );

      X = S.transpose() * P_ortho * W_mat * P_ortho * S;

      try{
        X_inv = X.inverse();
        // Equation no. 14 from Smith & Abel, 1987.
        pos = (0.5) * X_inv * ((S.transpose() * P_ortho * W_mat * P_ortho) * delta);
        // Account for normalized hydrophone positions
        pos = pos + pos_hydro_offset;
        // Save output
        ret.row(i) = (VectorXd) pos;
      }catch(const std::exception& ex){
        // Could not invert matrix, return NA values
        Rcpp::Rcout << "Exception Caught: " << ex.what() <<"\n";
        ret.row(i).fill(NA_REAL);
      }

    }else if(n_hydro == (n_dims + 1)){
      /*************************************************************************
       * Use normal form of spherical Interpolation
       * (only works for dims + 1 sensors)
       ************************************************************************/
      try{
        S_w = (S.transpose() * W_mat * S).inverse() * S.transpose() * W_mat;
        a = 4 - 4*(d.transpose() * S_w.transpose() * S_w * d).value();
        b = 4 * (d.transpose() * S_w.transpose() * S_w * delta).value();
        C = -(delta.transpose() * S_w.transpose() * S_w * delta).value();

        R_s_plus = (-b + sqrt(pow(b, 2) - 4*a*C)) / (2*a);
        R_s_min = (-b - sqrt(pow(b, 2) - 4*a*C)) / (2*a);

        if(R_s_plus > 0 && R_s_min > 0){
          // Ambiguous solution, return NA values
          ret.row(i).fill(NA_REAL);

        }else if(R_s_plus > 0){
          pos = 0.5*S_w*(delta - 2*R_s_plus*d);
          ret.row(i) =  (VectorXd) (pos + pos_hydro_offset);

        }else if(R_s_min > 0){
          pos = 0.5*S_w*(delta - 2*R_s_min*d);
          ret.row(i) =  (VectorXd) (pos + pos_hydro_offset);
        }
      }catch(const std::exception& ex){
        // Could not invert matrix, return NA values
        Rcpp::Rcout << "Exception Caught: " << ex.what() << "\n";
        ret.row(i).fill(NA_REAL);
      }
    }

    #ifdef PKGDEBUG
    if(i == 0 || i == (n_samples- 1) ){
      PRINT_HEADER("------ i: " << i);
      PRINT_SINGLE(n_hydro);
      PRINT_SINGLE(n_dims);
      PRINT_MULTI(pos_hydro_sub);
      PRINT_MULTI(toa_sub);
      PRINT_MULTI(W_sub);
      PRINT_SINGLE(n_delta);
      PRINT_MULTI(pos_hydro_offset);
      PRINT_MULTI(S);
      PRINT_MULTI(d);
      PRINT_MULTI(R);
      PRINT_MULTI(delta);
      PRINT_MULTI(I);
      PRINT_MULTI(W_mat);
      PRINT_MULTI(P_ortho);
      PRINT_MULTI(X);
      PRINT_MULTI(X_inv);
      PRINT_MULTI(pos);
      PRINT_MULTI(S_w);
      PRINT_SINGLE(a);
      PRINT_SINGLE(b);
      PRINT_SINGLE(C);
      PRINT_SINGLE(R_s_plus);
      PRINT_SINGLE(R_s_min);

    }
    #endif
  }
  return ret;
}

// The Jacobian of the Smith & Abel TDOA solution
Eigen::MatrixXd TDOA::smith_abel_J(
    const Eigen::VectorXd &toa,
    const Eigen::MatrixXd &pos_hydro,
    const double &c){

  using Eigen::MatrixXd;
  using Eigen::VectorXd;

  // ---- Prep input variables
  int N = pos_hydro.rows() - 1;
  int dims = pos_hydro.cols();
  // Relative positions of receivers
  MatrixXd S = MatrixXd(N, pos_hydro.cols());
  for(int i = 0; i < N; i++){
    S.row(i) = (pos_hydro.row(i+1) - pos_hydro.row(0));
  }
  // Distance differences from transmitter
  MatrixXd d = MatrixXd(N, 1);
  for(int i = 0; i < N; i++){
    d(i,0) = (toa(i+1) - toa(0)) * c;
  }
  // Receiver range differences from reference
  MatrixXd R = MatrixXd(N, 1);
  for(int i = 0; i < N; i++){
    R(i,0) = pow(pow(S.row(i).array(), 2).sum(), 0.5);
  }

  // Resulting Jacobian
  MatrixXd J = MatrixXd(dims, N);

  // Loop hydrophones and calculate derivative for each
  for(int i = 0; i < N; i++){

    double P_s = 1/pow(d.array(),2).sum();
    double P_ds = (-pow(d.array(),2).sum())*(2*d(i, 0));
    MatrixXd P_dd = MatrixXd(N,1); P_dd.fill(0); P_dd(i,0) = 1;
    MatrixXd P_B =  d.transpose() * P_s;
    MatrixXd P_dB = d.transpose()*P_ds + P_dd.transpose()*P_s;
    MatrixXd P_A = d * P_B;
    MatrixXd P_dA = (P_dd * P_B) + (d * P_dB);
    MatrixXd P = MatrixXd::Identity(N, N) - P_A;
    MatrixXd dP = -P_dA;
    MatrixXd B = P * P;
    MatrixXd dB = (P * dP) + (dP * P);
    MatrixXd v = (MatrixXd) (pow(R.array(), 2) - pow(d.array(), 2));
    MatrixXd dv = MatrixXd::Zero(N,1); dv(i,0) = -2*d(i,0);
    MatrixXd C = S.transpose() * B * v;
    MatrixXd dC = S.transpose() * ((B * dv) + (dB * v));
    MatrixXd A = S.transpose() * B * S;
    MatrixXd dA = S.transpose() * dB * S;
    MatrixXd A_inv = A.inverse();
    MatrixXd out = 0.5*((A_inv * dC) - (A_inv * dA * A_inv * C));

    J.col(i) = out;

    #ifdef PKGDEBUG
    PRINT_HEADER("------ smith_abel_J i = " << i);
    PRINT_SINGLE(P_s);
    PRINT_SINGLE(P_ds);
    PRINT_MULTI(P_dB);
    PRINT_MULTI(P_dA);
    PRINT_MULTI(P_dd);
    PRINT_MULTI(P);
    PRINT_MULTI(dP);
    PRINT_MULTI(B);
    PRINT_MULTI(dB);
    PRINT_MULTI(C);
    PRINT_MULTI(dC);
    PRINT_MULTI(A);
    PRINT_MULTI(dA);
    #endif
  }

  // Convert from delta d to delta t (from distance differences to arrival time differences)
  MatrixXd J2 = MatrixXd(dims, N+1);
  for(int i = 0; i < N; i++){
    J2.col(i+1) = J.col(i)*c;
  }
  // Create column for reference hydrophone
  for(int i = 0; i < dims; i++){
    J2(i, 0) = -J.row(i).sum()*c;
  }

  return(J2);

}
