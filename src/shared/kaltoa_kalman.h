#ifndef KALTOA_KALMAN_H
#define KALTOA_KALMAN_H

#ifndef TMB_HPP // TMB.hpp has no internal include guards
#include <TMB.hpp>
#define TMB_HPP
#endif

#include "kaltoa_process.h"
#include "kaltoa_observation.h"

namespace kaltoa_kalman{

  /* Return structure for Kalman functions
   * This holds all required data for applying a Kalman smoother, and optionally,
   * the marginal log likelihood.
   */
  template<class T>
  struct result{
    vector< matrix<T> > P_prior;
    vector< matrix<T> > P_post;
    matrix<T> x_prior;
    matrix<T> x_post;
    T nmll;
  };

  /*
   * This struct holds the parameters and functions for the process model of
   * the Kalman filter.  There are unique constructors for random walk and
   * continuous-time correlated random walk models.
   */
  enum model_PROCESS{RW, CTCRW};

  template<class Type>
  struct Process_model{
    Type sigma_pro, beta_pro;
    matrix<Type> F, Q;
    model_PROCESS model;
    int n_dims;
    int times_i;
    double sigma_trans;
    int n_states;

    // ------ Constructors
    // CTCRW
    Process_model(Type sigma, Type beta, int dims, double sigma_trns = 0){
      sigma_trans = sigma_trns;
      n_dims = dims;
      sigma_pro = sigma;
      beta_pro = beta;
      n_states = dims*2 + 1;
      F = matrix<Type>(n_states, n_states);
      Q = matrix<Type>(n_states, n_states);
      F.fill(0); Q.fill(0);
      model = CTCRW;

      // set transmission time variance
      Q(n_states-1, n_states-1) = pow(sigma_trns, 2);
    }
    // RW
    Process_model(Type sigma, int dims, double sigma_trns = 0){
      sigma_trans = sigma_trns;
      n_states = dims + 1;
      n_dims = dims;
      sigma_pro = sigma;
      Q = matrix<Type>(n_states, n_states);
      Q.fill(0);
      model = RW;

      // set transmission time variance
      Q(n_states-1, n_states-1) = pow(sigma_trns, 2);

    }

    // Update transformation and covariance matrices
    void update(Type delta){
      switch (model){
        case CTCRW: {
          CTCRW::f_F(F, beta_pro, delta, n_dims);
          CTCRW::f_Q(Q, sigma_pro, beta_pro, delta, n_dims);
        };break;
        case RW: {
          RW::f_Q(Q, sigma_pro, delta, n_dims);
        };break;
      }// end of switch-case
    }// end function

    // Predict next state
    vector<Type> predict_state(vector<Type> x_post_prev){
      vector<Type> ret;
      switch (model){
        case CTCRW: {
          ret = F * x_post_prev;
        };break;
        case RW: {
          ret = x_post_prev * 1;
          // set transmission time error to 0
        };break;
      }// end of switch-case

      ret(n_states - 1) = 0;
      return ret;
    }// end function

    // Prediction covariance
    matrix<Type> predict_covariance(matrix<Type> &P_post_prev){
      matrix<Type> ret;
      switch (model){
      case CTCRW: {
        ret = F * P_post_prev * F.transpose() + Q;
      };break;
      case RW: {
        ret = P_post_prev + Q;
      };break;
      }// end of switch-case
      return ret;
    }// end function

    // return position from state vector
    vector<Type> position(vector<Type> x){
      vector<Type> ret(n_dims);
      switch (model){
      case CTCRW: {
        for(int i = 0; i < n_dims; i++){
          ret(i) = x(i*2);
        }
      };break;
      case RW: {
        ret = x.segment(0, n_states - 1);
      };break;
      }// end of switch-case

      return ret;
    }

  };



  /*******************************************************************************
  * Kalman filter for time-of-arrival positioning and continuous-time
  * correlated-random-walk observation and process models, respectively.
  *
  * Process parameters (in struct object model_p)
  *  - sigma_pro: Variance parameter for CTCRW.
  *  - beta: Correlation parameter for CTCRW.
  *
  *  Data:
  *  - delta: Vector of time steps (in seconds).  First value is unused.
  *
  * Observation parameters
  *  - pos_hydro: Matrix holding the positions of thy hydrophones in meters.
  *      Columns are dimensions (x, y, and optionally z), rows are hydrophones.
  *  - sigma_obs: Variance parameter for Gaussian arrival time error.
  *  - toa: Time of arrival values for the hydrophone array.  Columns are
  *      hydrophones, rows are time indexes.
  *
  * Other parameters
  *  - init: A vector holding the initial state values following the format
  *  {position_x, velocity_x, position_y, velocity_y, ...}.
  *
  *  Return a struct holding the negative log likelihood along with values
  *  required for applying a kalman smoother.
  ******************************************************************************/

  template<class Type>
  result<Type> filter(
      Process_model<Type> model_p,
      const vector<Type> &transmission_times,
      const matrix<Type> &pos_hydro,
      const Type sigma_obs,
      const matrix<Type> &toa,
      const Type c,
      const vector<Type> &init_x,
      const vector<Type> &init_var,
      const Type sigma_emis = 0);

  /****************************************************************************
  ********************* Kalman helper function ********************************
  *****************************************************************************/

  // return index of largest value
  template<class Type>
  int max_idx(const vector<Type> v, const vector<int> ignore){

    // index for largest value
    int idx;

    for(int i = 0; i < v.size(); i++){
      if(i == 0){
        idx = 0;
      }else{
        if (v(i) > v(idx)) idx = i;
      }
    }
    return(idx);
  }

}

#endif
