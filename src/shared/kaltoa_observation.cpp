#include "kaltoa_observation.h"

/*******************************************************************************
 *  Functions for implementing time-of-arrival positioning in an Extended Kalman
 *   filter. See vignette for details.
 ******************************************************************************/

// Estimate signal travel times from tag and hydrophone array positions
template<class Type>
vector<Type> TOA::h(const vector<Type> &pos_tag,
                 const matrix<Type> &pos_hydro,
                 const Type &c){

  int n = pos_hydro.rows();  // # of hydrophones
  int dims = pos_tag.size();  // # of dimensions
  vector<Type> ret(n);         // Vector of travel times to return
  vector <Type> delta(dims);   // Distance between tag and hydrophone i

  // Loop hydrophones
  for(int i = 0; i < n; i++){
    delta = pos_tag - (vector<Type>)pos_hydro.row(i);
    // Calculate travel times
    ret(i) = (Type(1)/c) * sqrt( (pow(delta,Type(2))).sum() );
  }
  return ret;
}

template<class Type>
matrix<Type> TOA::h(const matrix<Type> &pos_tag,
                 const matrix<Type> &pos_hydro,
                 const Type &c){

  int n_tags = pos_tag.rows();
  int n_hydro = pos_hydro.rows();  // # of hydrophones
  matrix<Type> ret(n_tags, n_hydro);         // Vector of travel times to return
  vector<Type> tag;

  for(int i = 0; i < n_tags; i++){
    tag = pos_tag.row(i);
    ret.row(i) = TOA::h(tag , pos_hydro, c);
  }

  return ret;

}
/*  Populate a matrix with the closed-form solution to Jacobian of h().
*
* When pad_velocity is set to true, empty columns will be inserted representing
* the null effect that the velocity state has on the position process.
* This is required for using with an OU velocity process model in a CTCRW.
*/
template<class Type>
void TOA::f_H(matrix <Type> &mat,
              const matrix<Type> &pos_hydro,
              const vector<Type> &pos_tag,
              const Type &c,
              bool pad_velocity){

  int dims = pos_hydro.cols();
  int n = pos_hydro.rows();

  // ------ Populate matrix
  int col, col2;
  Type dk = Type(1);
  Type v_k, v_g, v_dg, v_dh;

  for (int r = 0; r < n; r++){
    // Loop matrix columns
    for (int d = 0; d < dims; d++){
      if (pad_velocity){col = d*2;}else{col = d;}

      v_k = pos_tag(d) - pos_hydro(r, d);

      v_g = pow(v_k, 2);
      for(int d2 = 0; d2 < dims; d2++){
        if (d != d2){
          v_g = v_g + pow(pos_tag(d2) -  pos_hydro(r, d2), 2);
        }
      }

      v_dg = Type(2) * v_k;

      v_dh = (Type(0.5)*(Type(1)/c) * pow(v_g,(-0.5)) ) * (v_dg) * (dk);

      mat(r, col) = v_dh;
    }
  }
}
