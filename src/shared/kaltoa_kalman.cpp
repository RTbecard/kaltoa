#include "kaltoa_process.h"
#include "kaltoa_observation.h"
#include "kaltoa_kalman.h"
#include "kaltoa_distributions.h"
#include "kaltoa_macros.h"


template<class Type>
kaltoa_kalman::result<Type> kaltoa_kalman::filter(
    kaltoa_kalman::Process_model<Type> model_p,
    const vector<Type> &transmission_times,
    const matrix<Type> &pos_hydro,
    const Type sigma_obs,
    const matrix<Type> &toa,
    const Type c,
    const vector<Type> &init_x,
    const vector<Type> &init_var,
    const Type sigma_emis){

  // int estimate_times = sigma_trns != 0;

  // ------ Initialize variables
  bool is_ctcrw = model_p.model == kaltoa_kalman::CTCRW;
  int n_dims = pos_hydro.cols();
  int n_time = transmission_times.size();
  int n_hydro = pos_hydro.rows();
  Type delta; // time since last transmission

  int dim_scale;
  if(is_ctcrw){dim_scale = 2;}else{dim_scale = 1;}

  // int n_states = n_dims*dim_scale + estimate_times;
  int n_states = n_dims*dim_scale + 1;

  // --- Prediction covariance
  // Prior
  vector< matrix<Type> > P_prior(n_time);
  P_prior(0) = matrix<Type>(n_states, n_states);
  P_prior(0).fill(0);
  // Posterior
  vector< matrix<Type> > P_post(n_time);
  // set first time step to initial covariance.
  P_post(0) = matrix<Type>(n_states, n_states);
  P_post(0).fill(0);
  for(int i = 0; i < n_states; i ++){
    P_post(0)(i,i)  = init_var(i);
    P_prior(0)(i,i)  = init_var(i);
  }

  // --- State Estimate
  // Prior
  matrix<Type> x_prior(n_time, n_states);
  x_prior.row(0) = init_x;
  // Posterior
  matrix<Type> x_post(n_time, n_states);
  x_post.row(0) = x_prior.row(0);

  // Observation covariance matrix (hydrophone detection error)
  matrix<Type> R = matrix<Type>::Identity(n_hydro, n_hydro)*pow(sigma_obs,2);
  // Innovation matrix
  matrix<Type> S = matrix<Type>(n_hydro, n_hydro);
  matrix<Type> S_inv = matrix<Type>(n_hydro, n_hydro);

  // Gain
  matrix<Type> K = matrix<Type>(n_states, n_states);
  // Identity matrix (for hydrophones)
  matrix <Type> I_hydro = matrix<Type>::Identity(n_hydro, n_hydro);
  // Identity matrix (for states)
  matrix <Type> I_state = matrix<Type>::Identity(n_states, n_states);
  // vector holding position
  vector<Type> pos(n_dims);
  // Jacobian of observation model
  matrix<Type> H = matrix<Type>(n_hydro, n_states);
  H.fill(0.0);
  // set column for transmission time derivative (1)
  H.col(n_states-1).fill(1.0);

  // Observed TOA vector
  vector<Type> z = vector<Type>(n_hydro);
  // Expected TOA vector
  vector <Type> h = vector<Type>(n_hydro);
  // Residual TOA vectors
  vector <Type> y_prior = vector<Type>(n_hydro);
  vector <Type> y_post = vector<Type>(n_hydro);
  vector <Type> x_post_outlier = vector<Type>(n_hydro);

  // Binary checking vectors
  vector<int> chk_nan(n_hydro);
  vector<int> chk_outlier(n_hydro);

  // Mahalanobis distance
  Type m;

  #ifdef PKGDEBUG
  PRINT_HEADER("======== kalman ========");
  PRINT_SINGLE(is_ctcrw);
  PRINT_SINGLE(dim_scale);
  PRINT_SINGLE(n_time);
  PRINT_MULTI(I_hydro);
  PRINT_MULTI(I_state);
  PRINT_MULTI(R);
  PRINT_SINGLE(model_p.sigma_pro);
  PRINT_SINGLE(sigma_emis);
  if(is_ctcrw){
    PRINT_SINGLE(model_p.beta_pro);
  }
  PRINT_SINGLE(sigma_obs);
  PRINT_MULTI(init_x);
  PRINT_MULTI(init_var);
  PRINT_MULTI(transmission_times);
  #endif

  Type nmll = 0;
  // 0 mean values for log likelihood calculation
  vector<Type> mu_det(n_hydro);
  mu_det.fill(0);

  // Loop time steps
  for(int i = 0; i < n_time; i ++){

    // ------ Predict step
    if(i > 0){
      delta = transmission_times(i) - transmission_times(i-1);
      // update F and Q
      model_p.update(delta);
      // Predict next step
      x_prior.row(i) = model_p.predict_state((vector<Type>) x_post.row(i-1));
      // Calculate prediction covariance
      P_prior(i) = model_p.predict_covariance(P_post(i-1));
    }

    // ------ Update step
    // Residual
    z = toa.row(i);
    // Position
    pos = model_p.position(x_prior.row(i));
    // Detection time
    h = TOA::h(pos, pos_hydro, c) + transmission_times(i);
    // updated state
    y_prior = z - h;

    // Update Jacobian
    TOA::f_H(H, pos_hydro, pos, c, is_ctcrw);
    H.col(n_states - 1).fill(1);  // transmission derivative

    // Deal with missing sensors
    for(int j = 0; j < n_hydro; j ++){
      if(isna(z(j))){
        chk_nan(j) = 1;
        // Zero-out missing observations
        // See 6.80 of Shumway & Stoffer (2000) DOI:10.1007/978-1-4757-3261-0_3
        y_prior(j) = 0;
        H.row(j).fill(0);
        R(j,j) = 1;
      }else{
        chk_nan(j) = 0;
        R(j,j) = pow(sigma_obs,2);
      }
    }

    // Propagate process covariance if no observations are made.
    // this will be overwritten if there are observations
    x_post.row(i) = x_prior.row(i);
    P_post(i) = P_prior(i);

    // Check for all observations being missed
    // at least two hydrophone detections required for position estimate
    if(chk_nan.sum() < (n_hydro - 1)){

      // Innovation
      S = (H * P_prior(i) * H.transpose()) + R;
      S_inv = S.inverse();

      // Marginal likelihood
      // 6.63 from Shumway & Stoffer (2000) DOI:10.1007/978-1-4757-3261-0_3
      nmll += 0.5 *
        ((y_prior.matrix().transpose() * (S_inv*y_prior.matrix())).sum() +
        log(S.determinant()));

      // ------ Calculate gain
      K = (P_prior(i) * H.transpose()) * S_inv;
      // Update state estimate
      x_post.row(i) = x_prior.row(i);
      x_post.row(i).array() += (K*y_prior).array();
      // Update covariance
      P_post(i) = (I_state - (K * H)) * P_prior(i);
    }

    #ifdef PKGDEBUG
    // Show first and last states
    if(i <= 3 || i == n_time - 1){
      PRINT_HEADER("-------- step " << i << " --------");
      PRINT_SINGLE(delta);
      PRINT_MULTI(chk_outlier);
      PRINT_MULTI(chk_nan);
      if(is_ctcrw){
        PRINT_MULTI(model_p.F);
      }
      PRINT_MULTI(model_p.Q);
      PRINT_MULTI(P_prior(i));
      PRINT_MULTI(P_post(i));
      PRINT_MULTI(x_prior.row(i));
      PRINT_MULTI(x_post.row(i));
      PRINT_MULTI(H);
      PRINT_MULTI(R);
      PRINT_MULTI(S);
      PRINT_MULTI(z);
      PRINT_MULTI(h);
      PRINT_MULTI(y_prior);
      PRINT_MULTI(K * H);
      PRINT_MULTI(S_inv);
      PRINT_MULTI(K);
      PRINT_MULTI(y_prior.matrix());
      PRINT_MULTI((y_prior.matrix().transpose() * (S_inv*y_prior.matrix())));
      PRINT_MULTI(nmll);
    }
    #endif
  }

  // Package results in struct
  kaltoa_kalman::result<Type> ret;
  ret = (kaltoa_kalman::result<Type>){
    .P_prior = P_prior,
    .P_post = P_post,
    .x_prior = x_prior,
    .x_post = x_post
    };

  ret.nmll = nmll;

  return ret;
}
