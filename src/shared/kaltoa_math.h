#ifndef KALTOA_MATH_H
#define KALTOA_MATH_H

#ifndef TMB_HPP // TMB.hpp has no internal include guards
#include <TMB.hpp>
#define TMB_HPP
#endif

// Find largest element in matrix
template<class Type>
Type max(matrix<Type> x);

template<class Type>
Type max(Type x, Type y);


// -------------------------- log domain math ----------------------------------
// log-sum-exponent trick.
// Sum log values in the linear domain while avoiding numerical errors.
template <class Type>
Type logSumExp(Type x, Type y);

template <class Type>
Type logSumExp(vector<Type> x);

template <class Type>
Type logSumExp(matrix<Type> x);

// Take the dot product in the linear domain
// This should be numerically stable.
template <class Type>
matrix<Type> logDotExp(matrix<Type> x, matrix<Type> y);

// --------------------------- Linear interpolation ----------------------------
template <class Type>
vector<Type> interp_linear(vector<Type> x_in, vector<Type> y_in, vector<Type> x_out);



#endif
