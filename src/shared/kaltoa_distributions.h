#ifndef KALTOA_DISTRIBUTIONS_H
#define KALTOA_DISTRIBUTIONS_H

#ifndef TMB_HPP // TMB.hpp has no internal include guards
#include <TMB.hpp>
#define TMB_HPP
#endif

namespace kaltoa_distributions{
/*
 * Generalized Normal Distribution.
 * See https://en.wikipedia.org/wiki/Generalized_normal_distribution
 * beta parameter controls the "sharpness" of distribution edges.  Can be used
 * as an approximation of a random uniform distribution.
 */


// ============ Convenience math functions
template <typename Type> int sign(Type val) {
        return (Type(0) < val) - (val < Type(0));}
template <typename T> T abs(const T& v) { return v < 0 ? -v : v;}

template<class Type>
Type dexpowdist(const Type &x, const Type &mu, const Type &alpha,
             const Type &beta, const int give_log = false);

// ============ Distribution functions
template<class Type>
Type igamma(const Type &a, const Type &x);

template<class Type>
Type pexpowdist(const Type &x, const Type &mu,
             const Type &alpha, const Type &beta);

template<class Type>
Type qexpowdist(const Type p, const Type mu, const Type alpha, const Type beta);

template<class Type>
vector<Type> dexpowdist(const vector<Type> &x, const Type &mu, const Type &alpha,
                     const Type &beta, const int give_log = false);

template<class Type>
vector<Type> pexpowdist(const vector<Type> &x, const Type &mu,
                     const Type &alpha, const Type &beta);

template<class Type>
vector<Type> qexpowdist(const vector<Type> p, const Type mu, const Type alpha, const Type beta);


template <class Type>
Type dmvtnorm(const vector<Type> x, const vector<Type> mu,
           const matrix<Type> Sigma, const int give_log = false);

template <class Type>
vector<Type> dmvtnorm(const matrix<Type> x, const vector<Type> mu,
           const matrix<Type> Sigma, const int give_log = false);


}


#endif
