#ifndef OBSERVATION_H // include guard
#define OBSERVATION_H

#ifndef TMB_HPP // TMB.hpp has no internal include guards
#include <TMB.hpp>
#define TMB_HPP
#endif

namespace TOA{
  // Estimate signal travel times times from tag and hydrophone array positions
  template<class Type>
  vector<Type> h(const vector<Type> &pos_tag,
                 const matrix<Type> &pos_hydro,
                 const Type &c);

// Estimate signal travel times times from tag and hydrophone array positions
// and emission time offset
  template<class Type>
  matrix<Type> h(const matrix<Type> &pos_tag,
                 const matrix<Type> &pos_hydro,
                 Type const &c);

  // Populate a matrix with the closed-form solution to Jacobian of h().
  template<class Type>
  void f_H(matrix <Type> &mat,
           const matrix<Type> &pos_hydro,
           const vector<Type> &pos_tag,
           const Type &c,
           bool pad_velocity = false);
}

#endif
