/*
 * A small subset of TMB which provides minimal definitions for vector and
 * matrix classes.  This allows us to use these TMB types outside of TMB
 * compiled code.  This is a copy of tmb_utilts.hpp, but cppAD dependancies
 * have been removed.
 *
 * The purpose of this is to allow the use of TMB code in RcppEigen.
 */
#ifndef TMB_CLASSES_H
#define TMB_CLASSES_H

#define TMB_HPP  // Prevent TMB.hpp from loading

#include <RcppEigen.h>

using namespace Eigen;

/* brief Vector class used by TMB.
 The TMB vector class is implemented as an Eigen Array of
 dynamic length. In particular, vectorized operations are inherited
 from the Eigen library.
 */
template <class Type>
struct vector : Array<Type,Dynamic,1>
{
  typedef Type value_type;
  typedef Array<Type,Dynamic,1> Base;
  vector(void):Base() {}

  template<class T1>
  vector(T1 x):Base(x) {}

  template<class T1, class T2>
  vector(T1 x, T2 y):Base(x,y) {}


  template<class T1>
  vector & operator= (const T1 & other)
  {
    this->Base::operator=(other);
    return *this;
  }

  // /* index-vector subset */
  // template <class T>
  // Type & operator()(T ind){
  //   return this->Base::operator()(ind);
  // }
  using Base::operator();

  // kasper: would be better with references, i.e. allow x(indvec)=y;
  vector<Type> operator()(vector<int> ind){
    vector<Type> ans(ind.size());
    for(int i=0;i<ind.size();i++)ans[i]=this->operator[](ind[i]);
    return ans;
  }

  vector<Type> vec() { return *this; }
};

/** \brief Matrix class used by TMB.

 The TMB matrix class is implemented as an Eigen Matrix of
 dynamic dimension. In particular, linear algebra methods are inherited
 from the Eigen library.
 */
template <class Type>
struct matrix : Matrix<Type,Dynamic,Dynamic>
{
  typedef Matrix<Type,Dynamic,Dynamic> Base;
  matrix(void):Base() {}
  template<class T1>
  matrix(T1 x):Base(x) {}
  template<class T1, class T2>
  matrix(T1 x, T2 y):Base(x,y) {}

  template<class T1>
  matrix & operator= (const T1 & other)
  {
    this->Base::operator=(other);
    return *this;
  }

  /**
   The vec operator stacks the matrix columns into a single vector.
   */
  vector<Type> vec(){
    Array<Type,Dynamic,Dynamic> a = this->array();
    a.resize(a.size(), 1);
    return a;
  }
};

template <class Type>
vector<vector<Type> > split(vector<Type> x, vector<int> fac) {
  if (x.size() != fac.size()) Rf_error("x and fac must have equal length.");
  int nlevels = 0;
  for (int i = 0; i < fac.size(); i++)
    if (fac[i] >= nlevels) nlevels = fac[i] + 1;
    vector<vector<Type> > ans(nlevels);
    vector<int> lngt(nlevels);
    lngt.setZero();
    for (int i = 0; i < fac.size(); i++) lngt[fac[i]]++;
    for (int i = 0; i < nlevels; i++) ans[i].resize(lngt[i]);
    lngt.setZero();
    for (int i = 0; i < fac.size(); i++) {
      ans[fac[i]][lngt[fac[i]]] = x[i];
      lngt[fac[i]]++;
    }
    return ans;
}

/** Sum of vector, matrix or array */
template <template <class> class Vector, class Type>
Type sum(Vector<Type> x) {
  return x.sum();
}

/** Matrix * vector

 Simplifies syntax in that .matrix() can be avoided. Recall: TMB type vector is of Eigen type Array.
 */
template <class Type>
vector<Type> operator*(matrix<Type> A, vector<Type> x) {
  return A * x.matrix();
}

/** SparseMatrix * vector */
template <class Type>
vector<Type> operator*(Eigen::SparseMatrix<Type> A, vector<Type> x) {
  return (A * x.matrix()).array();
}

/** Diff of vector

 Difference of vector elements just like diff in R, but only for
 vectors.
 */
template <class Type>
vector<Type> diff(vector<Type> x) {
  int n = x.size();
  vector<Type> ans(n - 1);
  for (int i = 0; i < n - 1; i++) ans[i] = x[i + 1] - x[i];
  return ans;
}

#endif
