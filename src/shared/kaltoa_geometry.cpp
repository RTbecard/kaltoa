#include "kaltoa_macros.h"
#include "kaltoa_geometry.h"

template<class Type>
matrix <Type> kaltoa_geometry::ortho_proj(
    matrix <Type> x1, matrix <Type> x2, matrix <Type> p){
  //*
  //* Project a point, p, onto the linesegment spanning the points x1 and x2.
  //*

  // Normalize coordinate system so x2 is @ 0,0
  matrix <Type> x2_norm = x2 - x1;
  matrix <Type> p_norm = p - x1;

  // Project point to line
  Type scale = (x2_norm.transpose() * x2_norm).sum(); // sum converts to scalar val
  matrix <Type> ret = ((x2_norm * x2_norm.transpose()) / scale) * p_norm;

  // Translate back to original coordinate system
  ret = ret + x1;

  return(ret);
}

template<class Type>
bool kaltoa_geometry::hline_intersect(
    matrix <Type> x1, matrix <Type> x2, matrix <Type> p){

  // y = mx + b, set y to 0 and check if within endpoints
  // m = slope
  // b = y - mx
  // x = (y - b)/m

  // Check if point is with y range of line segment
  bool chk = (p(1,0) >= x1(1,0) && p(1,0) < x2(1,0)) ||
    (p(1,0) < x1(1,0) && p(1,0) >= x2(1,0));
  if(chk == false){
    return(false);
  }

  // Calculate x intercept of line segment
  Type m, x, b;
  if(x2(0,0) == x1(0,0)){
    // vertical line
    x = x2(0,0);
  }else{
    m = (x2(1,0)  - x1(1,0)) / (x2(0,0)  - x1(0,0));
    b = x1(1,0) - (m*x1(0,0));
    x = (p(1,0) - b)/m;
  }

  // Return true if x intercept is on right side
  if(x > p(0,0)){
    return(true);
  }else{
    return(false);
  }
}

template <class Type>
bool kaltoa_geometry::on_line(matrix <Type> x1, matrix <Type> x2, matrix <Type> p_ortho){
  //*
  //* Check if projected point p_ortho lies within the endpoints of the line
  //* x1, and x2.
  //*

  bool ret = (p_ortho(0,0) >= x1(0,0) && p_ortho(0,0) < x2(0,0)) ||
    (p_ortho(0,0) < x1(0,0) && p_ortho(0,0) >= x2(0,0));

  bool ret2 = (p_ortho(1,0) >= x1(1,0) && p_ortho(1,0) < x2(1,0)) ||
    (p_ortho(1,0) < x1(1,0) && p_ortho(1,0) >= x2(1,0));


  return(ret && ret2);
}

template <class Type>
Type kaltoa_geometry::dist_from_poly(matrix <Type> &poly, matrix <Type> p){


  // Get number of line segments in polygon
  int n_seg = poly.cols() - 1;

  Type dist_to_edge, d1, d2, d;

  // Init holder for line end points
  matrix <Type> x1(2,1), x2(2,1), p_ortho(2,1);

  // Counter for number of line intersections.
  // This is used to check if our point is inside the polygon.
  // Number of polygon intersections from a horizontal line on the right side of
  // our point will be counted.
  int intersect_count = 0;
  bool online;

  // #ifdef PKGDEBUG
  // PRINT_SINGLE(p.transpose());
  // #endif

  // loop line segments
  for(int i = 0; i < n_seg; i++){

    //  Extract line segments
    x1 = poly.col(i);
    x2 = poly.col(i+1);

    // project point onto line
    p_ortho = ortho_proj(x1, x2, p);

    online = on_line(x1,x2, p_ortho);

    if(online){
      // Get distance from line
      d = sqrt((pow((p_ortho - p).array(),Type(2)).sum()));
    }else{
      // Get distance from points
      d1 = sqrt(pow((x1 - p).array(), Type(2)).sum());
      d2 = sqrt(pow((x2 - p).array(), Type(2)).sum());
      d = std::min(d1, d2);
    }

    // Save new closest distance to edge
    if(i == 0){
      dist_to_edge = d;
    }else if(d < dist_to_edge){
      dist_to_edge = d;
    }

    // check for intersection
    if(hline_intersect(x1, x2, p)){
      intersect_count ++;
    }

    // #ifdef PKGDEBUG
    //     PRINT_SINGLE(i);
    //     PRINT_SINGLE(x1.transpose());
    //     PRINT_SINGLE(x2.transpose());
    //     PRINT_SINGLE(p_ortho.transpose());
    //     PRINT_SINGLE(online);
    //     PRINT_SINGLE(d);
    //     PRINT_SINGLE(dist_to_edge);
    //     PRINT_SINGLE(intersect_count);
    // #endif

  }
  // if odd number of segments are crossed, point is inside poly (true)
  return(dist_to_edge * (intersect_count % 2 == 0));
}
