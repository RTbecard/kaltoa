// Printing macros
#define PRINT_SINGLE(x) std::cout <<" --- " << #x << ": " << x << "\n"; std::cout.flush();
#define PRINT_MULTI(x) std::cout << " --- " << #x << ":\n" << x << "\n"; std::cout.flush();
#define PRINT_HEADER(x) std::cout << x << "\n"; std::cout.flush();

