// #ifndef TMB_HPP // TMB.hpp has no internal include guards
// //#include <TMB.hpp>
// #include "kaltoa_tmb_classes.h"
// #define TMB_HPP
// #endif

#include "kaltoa_math.h"

template<class Type>
Type max(matrix<Type> x){

  vector<Type> y(x.rows());
  vector<Type> tmp(x.cols());

  for(int i = 0; i < x.rows(); i++){
    tmp = x.row(i);
    y(i) = max(tmp);
  }

  return(max(y));
}

template<class Type>
Type max(Type x, Type y){
  if(x > y){
    return(x);
  }else{
    return(y);
  }
}


// -------------------------- log domain math ----------------------------------
template <class Type>
Type logSumExp(Type x, Type y){
  Type c = max(x, y);
  return (c + log(exp(x - c) + exp(y - c)));
}

template <class Type>
Type logSumExp(vector<Type> x){
  Type c = max(x);
  return (c + log(exp(x - c).sum()));
}

template <class Type>
Type logSumExp(matrix<Type> x){
  Type c = max(x);
  return (c + log(exp(x.array() - c).sum()));
}

template <class Type>
matrix<Type> logDotExp(matrix<Type> x, matrix<Type> y){

  Type max_x = max(x);
  Type max_y = max(y);

  matrix <Type> x2e(x.rows(), x.cols());
  matrix <Type> y2e(y.rows(), y.cols());
  x2e = exp(x.array() - max_x);
  y2e = exp(y.array() - max_y);

  matrix <Type> x2y2(x.rows(), y.cols());
  x2y2 = log((x2e*y2e).array());

  return(max_x + max_y + x2y2.array());
}


