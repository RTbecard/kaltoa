#ifndef KALTOA_GEOMETRY_H
#define KALTOA_GEOMETRY_H

#ifndef TMB_HPP // TMB.hpp has no internal include guards
#include <TMB.hpp>
#define TMB_HPP
#endif

namespace kaltoa_geometry{

  template<class Type>
  matrix <Type> ortho_proj(
      matrix <Type> x1, matrix <Type> x2, matrix <Type> p);

  template<class Type>
  bool hline_intersect(matrix <Type> x1, matrix <Type> x2, matrix <Type> p);

  template <class Type>
  bool on_line(matrix <Type> x1, matrix <Type> x2, matrix <Type> p_ortho);

  template <class Type>
  Type dist_from_poly(matrix <Type> &poly, matrix <Type> p);
}

#endif
