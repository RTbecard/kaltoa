#ifndef PROCESS_H // include guard
#define PROCESS_H

#ifndef TMB_HPP // TMB.hpp has no internal include guards
#include <TMB.hpp>
#define TMB_HPP
#endif


namespace CTCRW{
  // Position estimate variance
  template<class T>
  T ou_var_mu(const T &sigma, const T &beta, const T &delta);
  // Velocity estimate covariance
  template<class T>
  T ou_var_v(const T &sigma, const T &beta, const T &delta);
  // position/velocity estimate covariance
  template<class T>
  T ou_cov(const T &sigma, const T &beta, const T &delta);
  /************************* Kalman Filter functions **************************/
  // Populate 4x4 covariance matrix "mat"
  template<class T>
  void f_Q(matrix<T> &mat, const T &sigma, const T &beta, const T &delta, const int dims);
  // Populate 4x4 Transformation Matrix "mat"
  template<class T>
  void f_F(matrix<T> &mat, const T &beta, const T &delta, const int dims);
}

namespace RW{
  // Populate 4x4 covariance matrix "mat"
  template<class T>
  void f_Q(matrix<T> &mat, const T &sigma, const T &delta, const int dims);
  // Populate 4x4 Transformation Matrix "mat"
  // template<class T>
  // void f_F(matrix<T> &mat, const int dims);
}
#endif
