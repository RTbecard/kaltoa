#ifndef TDOA_H // include guard
#define TDOA_H

#include <RcppEigen.h>

namespace TDOA{

  // TDOA positioning, SMith & Abel 1987
  Eigen::MatrixXd smith_abel(
    const Eigen::MatrixXd &toa,
    const Eigen::MatrixXd &pos_hydro,
    const double &c,
    const Eigen::VectorXd &W);

  // The Jacobian of the Smith & Abel TDOA solution
  Eigen::MatrixXd smith_abel_J(
    const Eigen::VectorXd &toa,
    const Eigen::MatrixXd &pos_hydro,
    const double &c);


}

#endif
