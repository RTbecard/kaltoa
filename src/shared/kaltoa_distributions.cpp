#include "kaltoa_distributions.h"

/*
 * Supplementary functions
 */
template<class Type>
Type kaltoa_distributions::igamma(const Type &a, const Type &x){
  return (Type) pgamma(x, a, Type(1)) * exp(lgamma(a));
}

/*******************************************************************************
 * Generalized Gaussian Distribution
 * (a.k.a. exponential power distribution)
 ******************************************************************************/
template<class Type>
Type kaltoa_distributions::dexpowdist(const Type &x, const Type &mu,
                           const Type &alpha, const Type &beta,
                           const int give_log){

  // Gamma term
  Type gma = (Type) exp(lgamma(Type(1)/beta));
  // Denominator term
  Type denom = Type(2)*alpha*gma;
  // Exponential term
  Type xpnt = exp(- pow( abs(x - mu)/alpha, beta) );
  Type ret = (beta/denom)*xpnt;

  if (give_log){
    return log(ret);
  }else{
    return ret;
  }
}

template<class Type>
Type kaltoa_distributions::pexpowdist(const Type &x, const Type &mu,
                           const Type &alpha, const Type &beta){

  //Type gma = (Type) gammafn((double) Type(1)/beta);
  Type gma = (Type) exp(lgamma(Type(1)/beta));

  // for igamma function, see R ?pgamma
  Type igma = (Type) igamma(Type(1)/beta ,pow(abs((x - mu)/alpha), beta));

  Type ret = Type(0.5) + sign(x - mu) * (Type(1)/(Type(2)*gma)) * igma;
  return ret;
}

template<class Type>
Type kaltoa_distributions::qexpowdist(const Type p, const Type mu,
                                   const Type alpha, const Type beta){

  Type F_inv = qgamma(Type(2)*abs(p - Type(0.5)),
                      Type(1)/beta,
                      Type(1)/pow(Type(1)/alpha, beta),
                      Type(1), Type(0));

  Type ret = sign(p - Type(0.5)) * pow(F_inv, Type(1)/beta)  + mu;

  return ret;
}


template<class Type>
vector<Type> kaltoa_distributions::dexpowdist(const vector<Type> &x, const Type &mu,
                                   const Type &alpha, const Type &beta,
                                   const int give_log){
  // Length of input vector
  int rows = x.rows();
  // Initialize return vector
  vector<Type> ret(rows);
  // Calculate values
  for(int r = 0; r < rows; r ++){
    ret(r) = dexpowdist(x(r), mu, alpha, beta, give_log);
  }

  if(give_log){
    return log(ret);
  }else{
    return ret;
  }
}

template<class Type>
vector<Type> kaltoa_distributions::pexpowdist(const vector<Type> &x, const Type &mu,
                                   const Type &alpha, const Type &beta){
  // Length of input vector
  int rows = x.rows();
  // Initialize return vector
  vector<Type> ret(rows);
  // Calculate values
  for(int r = 0; r < rows; r ++){
    ret(r) = pexpowdist(x(r), mu, alpha, beta);
  }
  return ret;
}

template<class Type>
vector<Type> kaltoa_distributions::qexpowdist(const vector<Type> p, const Type mu,
                                           const Type alpha, const Type beta){
  // Length of input vector
  int rows = p.rows();
  // Initialize return vector
  vector<Type> ret(rows);
  // Calculate values
  for(int r = 0; r < rows; r ++){
    ret(r) = qexpowdist(p(r), mu, alpha, beta);
  }
  return ret;
}


/*******************************************************************************
 * Multivariate normal distribution
 ******************************************************************************/
template <class Type>
Type kaltoa_distributions::dmvtnorm(const vector<Type> x, const vector<Type> mu,
                               const matrix<Type> Sigma, const int give_log){
  int k = x.size();

  if ( mu.size() != Sigma.rows()) {
    throw std::invalid_argument( "Invalid length for mu" );
  }
  if ( Sigma.cols() != Sigma.rows()) {
    throw std::invalid_argument( "Sigma must be square" );
  }

  Type Sigma_det = Sigma.determinant();
  matrix<Type> Sigma_inv = Sigma.inverse();
  Type p_1 = Type(1.0) / pow(Type(2.0)*M_PI, k/Type(2.0));
  Type p_2 = Type(1.0)/pow(Sigma_det, Type(0.5));

  vector<Type> resid = (x - mu);
  Type p_exp = Type(0.5) * (resid * (Sigma_inv * resid)).sum();
  Type p_3 = Type(1.0)/exp(p_exp);

  if (give_log){
    return p_1 * p_2 * p_3;
  }else{
    return log(p_1 * p_2 * p_3);
  }
}

template <class Type>
vector<Type> kaltoa_distributions::dmvtnorm(const matrix<Type> x, const vector<Type> mu,
                                 const matrix<Type> Sigma, const int give_log){
  int k = x.cols();
  int n = x.rows();

  if ( k != Sigma.rows()) {
    throw std::invalid_argument( "x must have same number of columns as Sigma" );
  }
  if ( mu.size() != Sigma.rows()) {
    throw std::invalid_argument( "Invalid length for mu" );
  }
  if ( Sigma.cols() != Sigma.rows()) {
    throw std::invalid_argument( "Sigma must be square" );
  }


  Type Sigma_det = Sigma.determinant();
  matrix<Type> Sigma_inv = Sigma.inverse();

  Type p_1 = Type(1.0) / pow(Type(2.0)*M_PI, k/Type(2.0));
  Type p_2 = Type(1.0) / pow(Sigma_det, Type(0.5));

  vector<Type> ret(n);

  vector<Type> resid;
  Type p_exp;
  Type p_3;

  for(int i = 0; i < n; i++){
    resid = ((vector<Type>)x.row(i) - mu);
    p_exp = Type(0.5) * (resid*(Sigma_inv*resid)).sum();
    p_3 = Type(1.0)/exp(p_exp);

    if(give_log){
      ret(i) = log(p_1 * p_2 * p_3);
    }else{
      ret(i) = p_1 * p_2 * p_3;
    }
  }

  return ret;
}
