#include "tmb_likelihoods.h"

#ifndef TMB_HPP // TMB.hpp has no internal include guards
#include <TMB.hpp>
#define TMB_HPP
#endif

#include "kaltoa_math.h"
#include "kaltoa_distributions.h"
#include "kaltoa_process.h"
#include "kaltoa_observation.h"
//#include "kaltoa_kalman.h"

// Allows the use of TMB macros DATA and PARAMETER outside main template function
#undef  TMB_OBJECTIVE_PTR
#define TMB_OBJECTIVE_PTR obj

/*****************************************************************************
 * Transmission interval models
 ****************************************************************************/

template<class Type>
Type kaltoa_likelihoods::emission_static(
    objective_function<Type> *obj,
    vector<Type> &emission_time){

  Type ll;

  int n_time = emission_time.size();

  // Parameters
  PARAMETER(ln_sigma_emis);
  Type sigma_emis = exp(ln_sigma_emis);
  REPORT(sigma_emis);
  //ADREPORT(sigma_emis);

  vector<Type> delta(n_time - 1);

  // likelihood
  for(int t = 1; t < n_time; t ++){
    delta(t-1) = emission_time(t) - emission_time(t - 1);
  }

  ll =  dnorm(delta, Type(0), sigma_emis, true).sum();

  #ifdef PKGDEBUG
  PRINT_SINGLE(ll);
  #endif

  return(ll);
}

template<class Type>
Type kaltoa_likelihoods::emission_uniform(
    objective_function<Type> *obj,
    vector<Type> &emission_time){

  Type ll, delta, p, q;

  int n_time = emission_time.size();

  DATA_SCALAR(min);
  DATA_SCALAR(max);

  Type mu_emis = (min + max) / 2;
  Type alpha = (max - min) / 2;

  // Parameters
  DATA_SCALAR(ln_beta_emis);
  Type beta_emis = exp(ln_beta_emis);
  REPORT(beta_emis);
  // ADREPORT(beta_emis);

  /* Likelihood
   * Map a Generalized normal distribution to a standard normal.
   */
  for(int t = 1; t < n_time; t++){
    delta = emission_time(t) - emission_time(t-1);
    p = kaltoa_distributions::pexpowdist(delta, mu_emis, alpha, beta_emis);
    // Convert to quantile for standard normal distribution
    q = qnorm(p, Type(0), Type(1));
    // Get standard normal likelihood from transformed distribution
    ll += dnorm(q, Type(0), Type(1), true);
  }

  #ifdef PKGDEBUG
  PRINT_SINGLE(ll);
  #endif

  return(ll);

}

template<class Type>
Type kaltoa_likelihoods::emission_set(
    objective_function<Type> *obj,
    vector<Type> &emission_time){

  Type ll;

  // numer of transmissio times
  int n_time = emission_time.size();

  // Data
  // User provided apriori transmission time values
  DATA_VECTOR(emission_time_init);

  // Parameters
  PARAMETER(ln_sigma_emis);
  Type sigma_emis = exp(ln_sigma_emis);
  REPORT(sigma_emis);
  //ADREPORT(sigma_emis);

  vector<Type> delta;
  delta = emission_time - emission_time_init;


  ll =  dnorm(delta, Type(0), sigma_emis, true).sum();

  #ifdef PKGDEBUG
  PRINT_SINGLE(n_time);
  PRINT_SINGLE(delta(0));
  PRINT_SINGLE(delta(n_time - 1));
  PRINT_SINGLE(sigma_emis);
  PRINT_SINGLE(ll);
  #endif

  return(ll);
}

/*****************************************************************************
 * Movement model
 ****************************************************************************/

template<class Type>
Type kaltoa_likelihoods::process_rw(
    objective_function<Type> *obj,
    vector<Type> &emission_time,
    matrix<Type> &position){

  Type ll = Type(0);

  int n_dims = position.cols();
  int n_time = position.rows();
  Type delta;

  // Fixed variable
  PARAMETER(ln_sigma_proc);
  Type sigma_proc = exp(ln_sigma_proc);
  REPORT(sigma_proc);

  // ------ Declarations
  vector<Type> x_prev(n_dims);  // State vector
  matrix<Type> Q(n_dims, n_dims);  // Covariance matrix

  Q.fill(Type(0));

  vector<Type> residual(n_dims);
  // ------ Loop time steps
  for(int t = 1; t < n_time; t ++){
    delta = emission_time(t) - emission_time(t-1);
    // Generate state matrix
//    x_prev = (vector<Type>) position.row(t-1);
    residual = (vector<Type>) (position.row(t) - position.row(t-1));
    // Update covariance matrix
    RW::f_Q(Q, sigma_proc, delta, n_dims);
    // Calculate likelihood of position parameters
    ll -= density::MVNORM(Q)(residual);

    #ifdef PKGDEBUG
    if(t == 1 || t == n_time - 1){
      PRINT_MULTI(residual);
      PRINT_MULTI(Q);
      PRINT_MULTI(delta);
    }
    #endif

  }

  #ifdef PKGDEBUG
  PRINT_SINGLE(sigma_proc);
  PRINT_SINGLE(ll);
  #endif


  return ll;

}


template<class Type>
Type kaltoa_likelihoods::process_ctcrw(
    objective_function<Type> *obj,
    vector<Type> &emission_time,
    matrix<Type> &position){

  Type ll = Type(0);

  int n_dims = position.cols();
  int n_time = position.rows();
  Type delta;

  // Random variables
  PARAMETER_MATRIX(velocity);
  REPORT(velocity);

  // Fixed variable
  PARAMETER(ln_sigma_proc);
  PARAMETER(ln_beta_proc);

  Type sigma_proc = exp(ln_sigma_proc);
  REPORT(sigma_proc);
  Type beta_proc = exp(ln_beta_proc);
  REPORT(beta_proc);

  // ----- Declarations
  vector<Type> x_prev(n_dims*2);
  vector<Type> x_crnt(n_dims*2);

  vector<Type> residual(n_dims*2);     // State vector
  matrix<Type> F(n_dims*2, n_dims*2);  // Transformation matrix
  matrix<Type> Q(n_dims*2, n_dims*2);  // Covariance matrix

  F.fill(Type(0)); Q.fill(Type(0));

  // ------ Loop time steps
  for(int t = 1; t < n_time; t ++){
    delta = emission_time(t) - emission_time(t-1);
    // Generate state matrix
    for(int d = 0; d < n_dims; d ++){
      x_prev(d*2) = position(t-1, d);
      x_prev(d*2 + 1) = velocity(t-1, d);
      x_crnt(d*2) = position(t, d);
      x_crnt(d*2 + 1) = velocity(t, d);
    }
    // Update transform and covariance matrices
    CTCRW::f_F(F, beta_proc, delta, n_dims);
    CTCRW::f_Q(Q, sigma_proc, beta_proc, delta, n_dims);
    // Estimate next state (position and velocity)
    residual = (F * x_prev) - x_crnt;
    // Calculate likelihood of position and velocity parameters
    ll -= density::MVNORM(Q)(residual);

    #ifdef PKGDEBUG
    if(t == 0 || t == n_time){
      PRINT_MULTI(x_prev);
      PRINT_MULTI(x_crnt);
      PRINT_MULTI(residual);
      PRINT_MULTI(Q);
      PRINT_MULTI(F);
      PRINT_SINGLE(delta);}
    #endif
  }


  #ifdef PKGDEBUG
  PRINT_SINGLE(sigma_proc);
  PRINT_SINGLE(beta_proc);
  PRINT_SINGLE(ll);
  #endif

  //ADREPORT(sigma_proc);
  //ADREPORT(beta_proc);
  //ADREPORT(velocity);

  return(ll);

}


/*****************************************************************************
 * TOA positioning
 ****************************************************************************/


/* -------------------------- Likelihood for TOAL ----------------------------*/
template<class Type>
Type kaltoa_likelihoods::toa(
    objective_function<Type> *obj,
    matrix<Type> &toa,
    matrix<Type> &pos_hydro,
    matrix<Type> &position,
    vector<Type> &emission_time,
    Type c){

  Type ll = Type(0);

  // toa residual variance
  PARAMETER(ln_sigma_obsv);
  Type sigma_obsv = exp(ln_sigma_obsv);
  // toa residual kurtosis
  PARAMETER(ln_tau_obsv);
  Type tau_obsv = exp(ln_tau_obsv);

  // ------ Telemetry likelihood
  // =========================================================================
  matrix<Type> transmission_time;
  transmission_time = TOA::h(position, pos_hydro, c);

  #ifdef PKGDEBUG
  PRINT_SINGLE(sigma_obsv);
  PRINT_SINGLE(tau_obsv);
  #endif

  int rows = toa.rows();
  int cols = toa.cols();

  for(int row = 0; row < rows; row ++){
    for(int col = 0; col < cols; col ++){
      // Skip missing detections
      if(!isna(toa(row, col))){

        ll += dSHASHo(
          transmission_time(row, col) + emission_time(row),
          toa(row, col),
          sigma_obsv,
          Type(0),
          tau_obsv, true);

        #ifdef PKGDEBUG
        if(row == 0 || row == rows - 1){
          PRINT_SINGLE(transmission_time(row, col) + emission_time(row) - toa(row, col));
        }
        #endif
      }
    }
  }

  #ifdef PKGDEBUG
  PRINT_SINGLE(ll);
  #endif

  REPORT(sigma_obsv);
  REPORT(tau_obsv);
  //ADREPORT(sigma_obsv);
  //ADREPORT(tau_obsv);

  return(ll);

}

/* ---------------- Likelihood for TOAL with HMM reflections -------------------
 *  A 2 state HHM is employed.  For the reflected state, a constant delay and
 *   variance is added to the reflected time of arrivals.  Since the latent
 *   state (reflected/direct) is not conditional on previous state values, we
 *   don't need the viterbi algorithm to make posterior state value estimates.
 */
// template<class Type>
// Type kaltoa_likelihoods::toa_reflect(
//     objective_function<Type> *obj,
//     matrix<Type> &toa,
//     matrix<Type> &pos_hydro,
//     matrix<Type> &position,
//     vector<Type> &emission_time,
//     Type c,
//     Type sigma_reflect){
//
//   Type ll = Type(0);
//
//   // toa residual variance
//   PARAMETER(ln_sigma_obsv);
//   Type sigma_obsv = exp(ln_sigma_obsv);
//   REPORT(sigma_obsv);
//   // toa residual kurtosis
//   PARAMETER(ln_tau_obsv);
//   Type tau_obsv = exp(ln_tau_obsv);
//   REPORT(tau_obsv);
//
//   // Reflection parameters
//   DATA_SCALAR(mu_reflect);
//   DATA_SCALAR(nu_reflect);
//   DATA_SCALAR(ln_tau_reflect);
//   Type tau_reflect = exp(ln_tau_reflect);
//
//   // ------ Telemetry likelihood
//   // =========================================================================
//   matrix<Type> transmission_time;
//
//   // estimate transmission times
//   transmission_time = TOA::h(position, pos_hydro, c);
//
//   #ifdef PKGDEBUG
//   PRINT_SINGLE(sigma_obsv);
//   PRINT_SINGLE(tau_obsv);
//   PRINT_SINGLE(mu_reflect);
//   PRINT_SINGLE(sigma_reflect);
//   #endif
//
//   matrix<Type> reflect_p(toa.rows(), toa.cols());
//   reflect_p.fill(0);
//
//   int rows = toa.rows();
//   int cols = toa.cols();
//
//   // TOA log densities
//   Type ld_direct = Type(0);
//   Type ld_reflec = Type(0);
//   Type ld_sum;
//   Type sigma_obsv_reflect;
//   Type delta_time;
//
//   for(int col = 0; col < cols; col ++){
//     for(int row = 1; row < rows; row ++){
//
//       // Skip missing detections
//       if(isna(toa(row, col))){
//         continue;
//       }
//
//       // ------ Emission probabilities
//       // Direct path emission
//       ld_direct = dSHASHo(
//         toa(row, col),
//         transmission_time(row, col) + emission_time(row),
//         sigma_obsv,
//         Type(0),
//         tau_obsv, true);
//       // Reflected path emission
//       ld_reflec = dSHASHo(
//         toa(row, col),
//         transmission_time(row, col) + emission_time(row) + mu_reflect,
//         sigma_reflect,
//         nu_reflect,
//         tau_reflect, true);
//
//       // ------ State probabilities
//       ld_sum = logSumExp(ld_reflec, ld_direct);
//
//       // Store point probability of state being a reflection
//       // I return a 1 or 0, rathert than the exact probability here because I
//       // was getting NaN values returned when reflections fell far outside the
//       // expected direct distribution
//       if(ld_reflec > ld_direct){
//         reflect_p(row, col) = 1;
//       }else{
//         reflect_p(row, col) = 0;
//       }
//
//       // ------ Likelihood is sum of emission probabilities.
//       // Each state has a 50/50 prior probability, hence the log(0.5)
//       ll += ld_sum + log(0.5);
//
//       #ifdef PKGDEBUG
//       if(row == 0 || row == rows){
//         PRINT_SINGLE(transmission_time(row, col) + emission_time(row) + mu_reflect - toa(row,col));
//         PRINT_SINGLE(transmission_time(row, col) + emission_time(row) - toa(row,col));
//       }
//       #endif
//     }
//    }
//
//   #ifdef PKGDEBUG
//   PRINT_SINGLE(ll);
//   #endif
//
//   REPORT(reflect_p);
//
//   return(ll);
//
// }

template<class Type>
Type kaltoa_likelihoods::toa_reflect(
    objective_function<Type> *obj,
    matrix<Type> &toa,
    matrix<Type> &pos_hydro,
    matrix<Type> &position,
    vector<Type> &emission_time,
    Type c, Type max_reflect){

  Type ll = Type(0);

  // toa residual variance
  PARAMETER(ln_sigma_obsv);
  Type sigma_obsv = exp(ln_sigma_obsv);
  REPORT(sigma_obsv);
  // toa residual kurtosis
  PARAMETER(ln_tau_obsv);
  Type tau_obsv = exp(ln_tau_obsv);
  REPORT(tau_obsv);

  // Reflection parameters
  DATA_SCALAR(beta_reflect);

  // ------ Telemetry likelihood
  // =========================================================================
  matrix<Type> transmission_time;

  // estimate transmission times
  transmission_time = TOA::h(position, pos_hydro, c);

#ifdef PKGDEBUG
  PRINT_SINGLE(sigma_obsv);
  PRINT_SINGLE(tau_obsv);
  PRINT_SINGLE(beta_reflect);
#endif

  matrix<Type> reflect_p(toa.rows(), toa.cols());
  reflect_p.fill(0);

  int rows = toa.rows();
  int cols = toa.cols();

  // TOA log densities
  Type ld_direct = Type(0);
  Type ld_reflec = Type(0);
  Type ld_sum;
  Type delta_time;

  for(int col = 0; col < cols; col ++){
    for(int row = 1; row < rows; row ++){

      // Skip missing detections
      if(isna(toa(row, col))){
        continue;
      }

      // ------ Emission probabilities
      // Direct path emission
      ld_direct = dSHASHo(
        toa(row, col), transmission_time(row, col) + emission_time(row),
        sigma_obsv,
        Type(0),
        tau_obsv, true);
      // Reflected path emission
      ld_reflec = kaltoa_distributions::dexpowdist(
        toa(row, col),
        transmission_time(row, col) + emission_time(row) + max_reflect/2,
        max_reflect/2,
        beta_reflect, true);

      // ------ Get mixture of densities
      ll += logSumExp(ld_reflec, ld_direct) + log(Type(0.5));

      // Store point probability of state being a reflection
      // I return a 1 or 0, rather than the exact probability here because I
      // was getting NaN values returned when reflections fell far outside the
      // expected direct distribution
      if(ld_reflec > ld_direct){
        reflect_p(row, col) = 1;
      }else{
        reflect_p(row, col) = 0;
      }

#ifdef PKGDEBUG
      if(row == 0 || row == rows){
        PRINT_SINGLE(transmission_time(row, col) + emission_time(row) + mu_reflect - toa(row,col));
        PRINT_SINGLE(transmission_time(row, col) + emission_time(row) - toa(row,col));
      }
#endif
    }
  }

#ifdef PKGDEBUG
  PRINT_SINGLE(ll);
#endif

  REPORT(reflect_p);

  return(ll);

}

#undef  TMB_OBJECTIVE_PTR
#define TMB_OBJECTIVE_PTR this
