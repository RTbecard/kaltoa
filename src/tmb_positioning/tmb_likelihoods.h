#ifndef TMB_LIKELIHOODS_H
#define TMB_LIKELIHOODS_H


#ifndef TMB_HPP // TMB.hpp has no internal include guards
#include <TMB.hpp>
#define TMB_HPP
#endif


namespace kaltoa_likelihoods{

  /*****************************************************************************
   * Transmission interval models
   ****************************************************************************/

  template<class Type>
  Type emission_static(
      objective_function<Type> *obj,
      vector<Type> &emission_time);

  template<class Type>
  Type emission_uniform(
      objective_function<Type> *obj,
      vector<Type> &emission_time);

  template<class Type>
  Type emission_set(
      objective_function<Type> *obj,
      vector<Type> &emission_time);

  /*****************************************************************************
   * Movement model
   ****************************************************************************/

  template<class Type>
  Type process_rw(
      objective_function<Type> *obj,
      vector<Type> &emission_time,
      matrix<Type> &position);

  template<class Type>
  Type process_ctcrw(
      objective_function<Type> *obj,
      vector<Type> &emission_time,
      matrix<Type> &position);

  /*****************************************************************************
   * TOA positioning
   ****************************************************************************/

  template<class Type>
  Type toa(
      objective_function<Type> *obj,
      matrix<Type> &toa,
      matrix<Type> &pos_hydro,
      matrix<Type> &position,
      vector<Type> &emission_time,
      Type c);

  template<class Type>
  Type toa_reflect(
      objective_function<Type> *obj,
      matrix<Type> &toa,
      matrix<Type> &pos_hydro,
      matrix<Type> &position,
      vector<Type> &emission_time,
      Type c, Type max_reflect);

}

#endif

