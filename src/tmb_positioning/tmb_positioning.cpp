
#ifndef TMB_HPP // TMB.hpp has no internal include guards
#include <TMB.hpp>
#define TMB_HPP
#endif


// Define NA function
#define isna(x) R_IsNA(asDouble(x))
#define isfinite(x) R_finite(asDouble(x))

// Macros for printing debug messages
#include "kaltoa_macros.h"
// TOA positioning model
#include "kaltoa_observation.cpp"
// Movement models
#include "kaltoa_process.cpp"
// Tag transmission and animal movement models
#include "tmb_likelihoods.cpp"
// Extra math functions for TMB
#include "kaltoa_math.cpp"
// Custom distributions
#include "kaltoa_distributions.cpp" // Must included after math namespaces


template<class Type>
Type objective_function<Type>::operator() () {

  // Input Data
  DATA_MATRIX(toa);
  DATA_MATRIX(pos_hydro);
  DATA_SCALAR(c);

  // Selectors
  DATA_INTEGER(i_emission);
  DATA_INTEGER(i_process);

  // Constants
  int n_time = toa.rows();
  int n_hydro = pos_hydro.rows();
  int n_dims = pos_hydro.cols();

  // Initialize negative log likelihood
  // parallel_accumulator<Type> nll(this);
  Type nll = Type(0);


  enum m_EMISSION{STATIC = 0, UNIFORM = 1, SET = 2};
  enum m_PROCESS{RW = 0, CTCRW = 1};


  #ifdef PKGDEBUG
  PRINT_HEADER("=== Transmission Likelihood ===");
  #endif
  /*****************************************************************************
  --------------------------Tag transmission time ------------------------------
  *****************************************************************************/
  // Get random transmission times
  PARAMETER_VECTOR(emission_time);
  REPORT(emission_time);
  //ADREPORT(transmission_time);

  switch(i_emission){
  case STATIC: {
    #ifdef PKGDEBUG
    PRINT_HEADER("Static");
    #endif

    // Static transmission intervals (with a Gaussian error dist.)
    nll -= kaltoa_likelihoods::emission_static(this, emission_time);

    };break;
  case UNIFORM: {
    #ifdef PKGDEBUG
    PRINT_HEADER("Uniform");
    #endif

    // Uniform random distribution of transmission intervals
    nll -= kaltoa_likelihoods::emission_uniform(this, emission_time);

    };break;
  case SET: {
    #ifdef PKGDEBUG
    PRINT_HEADER("Set");
    #endif

    // Transmission times are provided by user.  A Gaussian error will be
    // fitted to the provided transmission times
    nll -= kaltoa_likelihoods::emission_set(this, emission_time);

  };break;
  }

  // /**************************************************************************
  //  ------------------------ Telemetry positioning ---------------------------
  //  *************************************************************************/

  PARAMETER_MATRIX(position);
  REPORT(position);
  //ADREPORT(position);

  DATA_SCALAR(max_reflect);

  #ifdef PKGDEBUG
  PRINT_HEADER("=== Telemetry Likelihood ===");
  #endif
  // ------ Telemetry likelihood
  // =========================================================================
  if(max_reflect <= 0){
  // Do not consider reflected paths
    #ifdef PKGDEBUG
    PRINT_HEADER("toa direct");
    #endif

    nll -= kaltoa_likelihoods::toa(
      this, toa, pos_hydro, position, emission_time, c);

  }else{
    #ifdef PKGDEBUG
    PRINT_HEADER("toa reflect");
    #endif

    // Model possible reflected TOA values as a 2-state HMM
    nll -= kaltoa_likelihoods::toa_reflect(
      this, toa, pos_hydro, position, emission_time, c, max_reflect);

  }

  // ------ Position likelihood
  // =========================================================================
  #ifdef PKGDEBUG
  PRINT_HEADER("=== Position Likelihood ===");
  #endif
  switch(i_process){
  case RW: {
    #ifdef PKGDEBUG
    PRINT_HEADER("rw");
    #endif

    nll -= kaltoa_likelihoods::process_rw(this, emission_time, position);

  }; break;
  case CTCRW: {
    #ifdef PKGDEBUG
    PRINT_HEADER("crw");
    #endif

    nll -= kaltoa_likelihoods::process_ctcrw(this, emission_time, position);

  }; break;
  } // end of switch-case

  #ifdef PKGDEBUG
  PRINT_SINGLE(nll);
  #endif

  return nll;

}
