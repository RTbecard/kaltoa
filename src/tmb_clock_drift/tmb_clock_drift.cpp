/*
 * This model will take sync-tag emission and detection times between a
 * receiver pair as input.  It will try to normalize these times, resulting in
* 0 seconds between an emission and detection
 */


#ifndef TMB_HPP // TMB.hpp has no internal include guards
#include <TMB.hpp>
#define TMB_HPP
#endif

// Define NA function
#define isna(x) R_IsNA(asDouble(x))
#define isfinite(x) R_finite(asDouble(x))

// Macros for printing debug messages
#include "kaltoa_macros.h"
// Extra math functions for TMB
#include "kaltoa_math.cpp"
// Custom distributions
#include "kaltoa_distributions.cpp" // Must included after math namespaces

using kaltoa_distributions::dexpowdist;

template<class Type>
Type objective_function<Type>::operator() () {

  // ------ Input Data
  // sync-tag emission times
  DATA_VECTOR(emis);
  // sync-tag detection times
  DATA_VECTOR(det);

  DATA_SCALAR(c);
  DATA_SCALAR(min_interval);
  DATA_SCALAR(max_interval);

  // Constants
  int emis_n = emis.size();
  int det_n = det.size();

  PARAMETER_VECTOR(offset_values);
  DATA_VECTOR(offset_times);
  int offsets_n = offset_times.size();

  // AR(1) parameters for offset values
  PARAMETER(rw_sigma);

  // Parameters for detection likelihood values
  PARAMETER(det_sigma);
  DATA_SCALAR(det_beta);

#ifdef PKGDEBUG
  PRINT_SINGLE(c);
  PRINT_SINGLE(min_interval);
  PRINT_SINGLE(max_interval);
  PRINT_SINGLE(offsets_n);
  PRINT_SINGLE(rw_sigma);
  PRINT_SINGLE(det_sigma);
  PRINT_SINGLE(det_beta);
  PRINT_SINGLE(emis_n);
  PRINT_SINGLE(det_n);
#endif

  // Initialize negative log likelihood
  // parallel_accumulator<Type> nll(this);
  Type nll = Type(0);

  Type offset_resid_1, offset_resid_2;

  vector<Type> det_corr;

  // ---------------------- AR(1) likelihood of offsets ------------------------
  // Loop offsets
  for(int i = 1; i < offsets_n; i++){
    nll -= dnorm(offset_values[i], offset_values[i-1], rw_sigma, 1);
  }

  PRINT_SINGLE(nll);

  // Use linear interpolation to apply drift offsets to detentions
  det_corr = interp_linear(offset_times, offset_values, det) + det;

  // --------------------Get detection likelihoods -----------------------------
  // Only calculate likelihoods for detections that are within 2x the max
  // emissions interval.
  // We assume that any points outside this interval have effectively
  int det_i_start = 0;
  int det_i_end = 0;
  Type dens, dens_dnorm, dens_dexpo;

  // Loop emissions
  for(int emis_i = 0; emis_i < emis_n; emis_i++){


    // // Find first detection in bounds
    // while(det_i_start < det_n - 1){
    //   if(det_corr(det_i_start) < (emis(emis_i) - 2*max_interval)){
    //     det_i_start++;
    //   }else{
    //     break;
    //   }
    // }
    //
    // // Find last detection in bounds
    // while(det_i_end < det_n - 1){
    //   if(det_corr(det_i_end) < (emis(emis_i) + 2*max_interval)){
    //     det_i_end++;
    //   }else{
    //     det_i_end--;
    //     break;
    //   }
    // }

    // If no detection, set likelihood to a single detection at the end of
    // the boundary (2x max interval before the emission)
    // if(det_i_start >= det_i_end){
    //   dens = dnorm(-max_interval*Type(2), Type(0), det_sigma, 0);
    //   dens += dexpowdist(
    //     -max_interval * Type(2),
    //     min_interval / Type(2),
    //     min_interval / Type(2),
    //     det_beta,
    //     0);
    //   nll += log(dens);
    //   continue;
    // }

    // If multiple detections in bounds, sum probabilities (probability that
    // either detection is the true matching detection).
    dens = Type(0);
    for(int det_i = 0; det_i < det_n; det_i++){
      // Gaussian likelihood
      dens_dnorm = dnorm(emis(emis_i), det_corr(det_i), det_sigma, 0);
      // Ex Pow Likelihood (smooth approximation to a random uniform)
      dens_dexpo =+ dexpowdist(
        det_corr(det_i) - emis(emis_i),
        max_interval / Type(2),
        max_interval / Type(2),
        det_beta,
        0);
      dens += dens_dnorm + (dens_dexpo + dens_dnorm) - (dens_dexpo + dens_dnorm)*dens;
    }
    if(dens < 1e-12){
      dens = 1e-12;
    }
    nll -= log(dens);

#ifdef PKGDEBUG
    // debug statement for every 25th iteration
    if(emis_i % 25 == 0){
      PRINT_SINGLE(emis_i);
      PRINT_SINGLE(emis(emis_i));
      PRINT_SINGLE(-log(dens));
      PRINT_SINGLE(nll);
    }
#endif

  }


#ifdef PKGDEBUG
  PRINT_SINGLE(nll);
#endif

  return nll;

}
