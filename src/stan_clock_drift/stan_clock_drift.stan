
functions {

  // Linear interpolation
  // This is for calculating the new drift-corrected detection times
  real[] interp_linear(real[] x_in, real[] y_in, real[] x_out){

    // Init indicies
    int in_i = 1;
    int in_n = size(x_in);
    int out_n = size(x_out);

    // init output vector (interpoalted values)
    real y_out[out_n];

    real scale;

    // Loop output values
    for(out_i in 1:(out_n)){

      // Find next matching output indicie
      while(x_in[in_i] < x_out[out_i] && in_i < in_n){
        in_i += 1;
      }

      if(in_i == 1){
        // out_i is at beginning
        y_out[out_i] = y_in[1];
      }else if(out_i == out_n){
        // out_i is at end
        y_out[out_i] = y_in[in_n];
      }else{
        // interpolate
        scale = (x_out[out_i] - x_in[in_i - 1]) / (x_in[in_i] - x_in[in_i - 1]);
        y_out[out_i] = y_in[in_i - 1] + (y_in[in_i] - y_in[in_i - 1])*scale;
      }
    }

    return(y_out);

  }

  real detection_lpdf(real x, real mu, real det_sigma, real max_transmission_latency){
    real ret;
    ret = log_mix(
      0.5,
      normal_lpdf(x | mu, det_sigma),
      normal_lpdf(x | mu + max_transmission_latency/2, max_transmission_latency/2));
    return(ret);
  }


}

// The input data is a vector 'y' of length 'N'.
data {

  // Size of data vectors/arrays
  int<lower=0> N_emis;
  int<lower=0> N_det;
  int<lower=0> N_offset;

  real drift_prior_sigma;
  real max_transmission_latency;

  real clock_drift_init[N_offset];

  real emis[N_emis]; // array of emission times
  real det[N_det];   // array of detection times

  real max_interval; // maximum inter-emission interval

  real clock_time[N_offset];  //

  // ------ hyper parameters
  // offset sigma
  real drift_sigma_alpha;
  real drift_sigma_beta;
  // detection sigma
  real det_sigma_alpha;
  real det_sigma_beta;

  // print("det_sigma_alpha:", det_sigma_alpha);
  // print("det_sigma_beta:", det_sigma_beta);
  // print("max_interval:", max_interval);
  // print("offset_max: ", offset_max);

}

transformed data{

  // Find starting and ending indicies for emissions
  // This is dependant on the maximum transmission latency.
  int det_i_start;
  int det_i_end;

  for(i in 1:N_det){
    if(det[i] >= det[1] + max_transmission_latency){
      det_i_start = i;
      break;
    }
  }

  for(i in 1:N_det){
    if(det[i] >= det[N_det] - max_transmission_latency){
      det_i_end = i;
      break;
    }
  }
  det_i_end -= 1;

  // print("emis_i_start: ", det_i_start);
  // print("emis_i_end: ", det_i_end);
  // print("det: ", det);
  // print("offset_times: ", offset_times);


}

// The parameters accepted by the model. Our model
// accepts two parameters 'mu' and 'sigma'.
parameters {

  // Drift offsets
  real clock_drift[N_offset];

  // RW parameter for offset values
  real<lower=0> drift_sigma;

  // Parameters for detection likelihood values
  real<lower=0> det_sigma;

  // print("offset_values: ", offset_values);
  // print("offset_sigma:", offset_sigma);
  // print("det_sigma:", det_sigma);

}

transformed parameters{



}


model {

  // temp variables for detection likelihood
  real det_nll_norm;
  real det_nll_uniform;
  real det_nll_mix;
  real time_delta;

  real det_nll_tmp;
  real det_nll;

  real det_floor_ll;

  int emis_i_start = 1;
  int emis_i_end = 1;


  // ------ Apply linear interpolation (calcualte corrected deteciton times)
  real det_corr[N_det];
  det_corr = to_array_1d(
    to_vector(det) +
    to_vector(
      interp_linear(clock_time, clock_drift, det)));


  // Prior likelihood of offset random walk values
  drift_sigma ~ inv_gamma(drift_sigma_alpha, drift_sigma_beta);
  // print("Prior offset log density:                 ", target());

  // ------ Loop thorough clock drift values
  for(i in 1:N_offset){

    // Prior likelihood of each offset value
    clock_drift[i] ~ normal(clock_drift_init[i], drift_prior_sigma);

    // Posterior likelihood of each offset value (random walk)
    if(i > 1){
      time_delta = clock_time[i] - clock_time[i - 1];
      clock_drift[i] ~ normal(clock_drift[i - 1], drift_sigma * (time_delta/3600));
    }

  }
  // print("Posterior offest random walk log density: ", target());

  // Prior likelihood of detection sigma
  det_sigma ~ inv_gamma(det_sigma_alpha, det_sigma_beta);
  // print("Prior offset log density:                 ", target());

  // Calculate deteciton floor likelihood
  det_floor_ll = detection_lpdf(-max_interval*2 | 0, det_sigma, max_transmission_latency);

  // ------ Posterior likelihood of each detection
  for(det_i in det_i_start:det_i_end){
    // we'll calculate the probability that ANY of the detections are a match
    // for the current emission.  That means we'll sum the densities of all
    // detections calculated for each given emission.

    // likelihood for all emissions outside range
    // (floor likelihood times the number emissions outside max_interval*2)
    // This approximation GREATLY speeds up processing times, by skipping
    // density calculatioons that have virtually 0 impact ion the resulting
    // likelihood.
    det_nll = det_floor_ll + log(N_det - det_i_end - det_i_start + 1);

    // Find indicies of emissions within max_interval*2 of this detections
    while(emis_i_start < N_emis){
      if(emis[emis_i_start] < det[det_i] - max_interval*2){
        emis_i_start += 1;
      }else{
        break;
      }
    }
    while(emis_i_end < (N_emis - 1)){
      if(emis[emis_i_end + 1] < det[det_i] + max_interval*2){
        emis_i_end += 1;
      }else{
        break;
      }
    }

    // Skip this iteration if no emissions fall within the detection range
    if(emis_i_end < emis_i_start) continue;

    // For all emissions falling more than 2*max_interval outside of the
    // detection, assign a likelihood floor assuming they are all 2*max_interval

    // Loop over detections and sum likelihoods
    // The probability of ANY of the emissions being matched with the given detection.
    for(emis_i in emis_i_start:emis_i_end){

      // Calculate mixture likelihood of detection matching with this emission
      det_nll_mix = detection_lpdf(det_corr[det_i] | emis[emis_i], det_sigma, max_transmission_latency);
      // sum detection likelihoods
      det_nll = log_sum_exp(det_nll, det_nll_mix);

    }

    target += det_nll;

  }

  // print("expopow test: ", expowdist_lpdf(det_corr[N_det] | emis[N_emis] + max_interval/2, max_interval/2, det_beta));
  // print("det_corr[det_i]:                          ", det_corr[N_det]);
  // print("emis[emis_i]:                             ", emis[N_emis]);
  // print("det_nll_norm:                             ", det_nll_norm);
  // print("det_nll_uniform:                          ", det_nll_uniform);
  // print("det_nll_mix:                              ", det_nll_mix);
  // print("det_nll_tmp:                              ", det_nll_tmp);
  //
  // print("det_nll:                                  ", det_nll);

  // print("Posterior detections log density:         ", target());

}

