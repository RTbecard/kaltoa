/*
 * RcppEigen wrappers for TMB functions.
 *
 * This exports our TMB template functions for use in R.
 */

#include <RcppEigen.h>
// [[Rcpp::depends(RcppEigen)]

// Trigger include guard (do not include TMB.hpp)
#include "kaltoa_tmb_classes.h"

// Define NA function
#define isna(x) std::isnan(x)

// TMB types
#include "kaltoa_observation.cpp"
#include "kaltoa_process.cpp"
#include "kaltoa_kalman.cpp"
#include "kaltoa_tdoa.cpp"
#include "kaltoa_math.cpp"


// kaltoa_distributions uses math functions from a different name space
// depending on if called by Rcpp or TMB.  Need to define these functions before
// including the code to imitate TMB style.
using R::qgamma;
#define pgamma(a,b,c) R::pgamma(a,b,c,true,false)
#include "kaltoa_distributions.cpp"

using namespace Rcpp;

// Eigen types
using Eigen::VectorXd;
using Eigen::MatrixXd;

/*******************************************************************************
 * Time of arrival calculations
 ******************************************************************************/

//' cpp Time of arrival
//'
//' Generate expected arrival times for a signal emitted in a hydrophone array.
//'
//' @param pos_tag Matrix holding the tag locations.
//' Columns are spatial dimensions.
//' @param pos_hydro Matrix holding the hydrophone locations.
//' Columns are spatial dimensions.
//' @param c Speed of signal propagation in the medium.
//'
//[[Rcpp::export]]
Eigen::MatrixXd cpp_toa(
    const Eigen::MatrixXd pos_tag,
    const Eigen::MatrixXd pos_hydro,
    const double c) {

  // Run TMB function
  MatrixXd ret = TOA::h(
    (matrix<double>) pos_tag,
    (matrix<double>) pos_hydro,
    c);

  return ret;
}

/*******************************************************************************
 * Kalman filter
 ******************************************************************************/
// Convenience function for flattening vector of matrices (TMB cant handle
// 3D arrays) into a vector for output to R.  Flattening is column-major.
template<class Type>
vector<Type> flatten_vec_mat(vector<matrix <Type> > x){

  int n_x =  x.size();
  int rows_x = x(0).rows();
  int cols_x = x(0).cols();

  int n_out = cols_x * rows_x * n_x;
  vector<Type> ret(n_out);
  ret.fill(0);

  int j;
  for(int i = 0; i < n_x; i++){
    for(int col = 0; col < cols_x; col++){
      for(int row = 0; row < rows_x; row++){
        // Vector index
        j = i*rows_x*cols_x + col*rows_x + row;
        // Copy values to return vector
        ret(j) = x(i)(row, col);
      }
    }
  }
  return ret;
}

//' cpp Kalman Filter for Time of Arrival Positioning
//'
//' An extended Kalman filter which implements random walk, or continuous-time correlated random walk process models with a time of arrival observation model.
//'
//' @param sigma_pro Step length variance.
//' @param beta_pro Step length correlation.
//' @param transmission_times Vector of tag transmission times.
//' i.e. Time since last tag transmission.
//' @param pos_hydro Matrix holding the hydrophone locations.
//' Columns are spatial dimensions.
//' @param sigma_obs Time of arrival detection residual variance.
//' @param toa Time of arrival detection matrix.
//' @param c Speed of signal propagation in the medium.
//' @param init_x A vector of starting values: [x_position, x_velocity, y_position, y_velocity, ...].
//' Note, velocity states are only requires when the `beta_pro` parameter is set, otherwise this should be a vector of starting positions.
//' @param init_var A vector of starting variances for the initial states.
//' Typically, this should be set rather high to reflect the uncertainty in the starting positions.
//' @param give_nmll Return the negative marginal log likelihood.
//'
//'
//' @details Setting `beta_pro = NA` will result in a random walk model.
//' Opting for a continuous-time correlated random walk will also estimate the velocity states of the process.
//' Partially missing detections are allowed.
//' `NA` values for a given time step will be ignored from the update equations.
//' See the vignette `vignette("kalman_toa", package = 'kaltoa')` for a detailed rundown of how this function operates.
//'
//'
//[[Rcpp::export]]
List cpp_kalman(
    const double sigma_pro,
    const double beta_pro,
    const Eigen::VectorXd emission_times,
    const Eigen::MatrixXd pos_hydro,
    const double sigma_obs,
    const Eigen::MatrixXd toa,
    const double c,
    const Eigen::VectorXd init_x,
    const Eigen::VectorXd init_sigma,
    const double sigma_emis = 0) {

  std::string type;

  int estimate_times = sigma_emis != 0;

  // Init process model
  kaltoa_kalman::Process_model<double>* model_p_ptr;
  if (std::isnan(beta_pro)){
    model_p_ptr = new kaltoa_kalman::Process_model<double>(
      sigma_pro, (int) pos_hydro.cols(), (double) sigma_emis);
    type = std::string("RW");
  }else{
    model_p_ptr = new kaltoa_kalman::Process_model<double>(
      sigma_pro, beta_pro, (int) pos_hydro.cols(), (double) sigma_emis);
    type = std::string("CTCRW");
  }

  Eigen::VectorXd init_var = init_sigma.array()*init_sigma.array();

  // Init result variable (struct of matrices)
  kaltoa_kalman::result<double> res;
  // Run filter
  res = kaltoa_kalman::filter(
    *model_p_ptr,
    (vector<double>) emission_times,
    (matrix<double>) pos_hydro,
    sigma_obs,
    (matrix<double>) toa, c,
    (vector<double>) init_x,
    (vector<double>) init_var,
    sigma_emis);

  delete model_p_ptr;

  // Flatten covariance results into a vector.
  // Will expand into an array later in R.
  VectorXd v_P_prior, v_P_post;
  v_P_prior =  flatten_vec_mat(res.P_prior);
  v_P_post = flatten_vec_mat(res.P_post);

  List l_ret;
  l_ret = List::create(
    Named("process_model") = type,
    Named("beta_pro") = (double) beta_pro,
    Named("sigma_pro") = (double) sigma_pro,
    Named("sigma_obs") = (double) sigma_obs,
    Named("sigma_emis") = (double) sigma_emis,
    Named("transmission_times") = (VectorXd) emission_times,
    Named("x_prior") = (MatrixXd) res.x_prior,
    Named("x_post") = (MatrixXd) res.x_post,
    Named("P_prior") =  (VectorXd) v_P_prior,
    Named("P_post") = (VectorXd) v_P_post,
    Named("nmll") = (double) res.nmll);

  // set class
  l_ret.attr("class") = std::string("KaltoaKalman");

  return l_ret;
}

/*******************************************************************************
 * Generalized Gaussian distribution for time-of-arrival error
 ******************************************************************************/

//' Exponential power distribution.
//'
//' A generalization of the normal distribution with added skewness and kurtosis parameters.
//' Density, cumulative probability, and quantile functions are provided.
//' See wikipedia for [more details](https://en.wikipedia.org/wiki/Generalized_normal_distribution#Version_1).
//'
//' @param mu Vector of mean values.
//' @param alpha Scale parameter.
//' @param beta Shape parameter.
//'
//' @rdname expowdist
//[[Rcpp::export]]
Eigen::VectorXd cpp_dexpowdist(
    const Eigen::VectorXd x, const double mu,
    const double alpha, const double beta,
    const int give_log = false) {

  // Run TMB function
  Eigen::VectorXd ret;
  ret = (VectorXd) kaltoa_distributions::dexpowdist(
    (vector<double>) x, mu, alpha, beta, give_log);
  return ret;
}

/***R
x <- seq(-3, 3, 0.01)
p <- cpp_dexpowdist(x, 0, 1.3, 10)

plot(x, p, t = 'l',
     xlab = "Quantile",
     ylab = "Density",
     main = "Expoonetial Power Distribution")
abline(v = c(-1, 1), col = 'red', lt = 2)

*/

//' @rdname expowdist
//[[Rcpp::export]]
Eigen::VectorXd cpp_pexpowdist(
    const Eigen::VectorXd x, const double mu,
    const double alpha, const double beta) {

  // Run TMB function
  Eigen::VectorXd ret;
  ret = (VectorXd) kaltoa_distributions::pexpowdist(
    (vector<double>) x, mu, alpha, beta);
  return ret;
}
/***R
x <- seq(-3, 3, 0.01)
p <- cpp_pexpowdist(x, 0, 1, 0.5)

plot(x, p, t = 'l',
     xlab = "Quantile",
     ylab = "Cumul. Prob.",
     main = "Exponetial Power Distribution")
*/

//' @rdname expowdist
//[[Rcpp::export]]
Eigen::VectorXd cpp_qexpowdist(
    const Eigen::VectorXd p, const double mu,
    const double alpha, const double beta) {


  // Run TMB function
  Eigen::VectorXd ret;
  ret = (VectorXd) kaltoa_distributions::qexpowdist(
    (vector<double>) p, mu, alpha, beta);
  return ret;
}
/***R
p <- seq(0.1, 0.9, 0.001)
q <- cpp_qexpowdist(p, 0, 1, 0.5)

plot(q, p, t = 'l',
     xlab = "Quantile",
     ylab = "Cumul. Prob.",
     main = "Expoonetial Power Distribution")
  */

//' @rdname expowdist
//[[Rcpp::export]]
Eigen::VectorXd cpp_dmvtnorm(
    const Eigen::MatrixXd x, const Eigen::VectorXd mu,
    const Eigen::MatrixXd Sigma, const int give_log = false){

  Eigen::VectorXd ret = kaltoa_distributions::dmvtnorm(
    (matrix<double>) x,
    (vector<double>) mu,
    (matrix<double>) Sigma,
    give_log);

  return ret;
}

/***R
require(mvtnorm)

mean = c(0,0,0)
sigma = diag(3)
x = rmvnorm(1, mean, sigma)

dmvnorm(x)
cpp_dmvtnorm(x, mean, sigma)
  */

/*******************************************************************************
 * Closed form solution to Time-difference of arrival positioning
 ******************************************************************************/

//' @title Time-difference of Arrival Positioning
//' @rdname tdoa_methods
//' @name tdoa_methods
//'
//' @description This implements the closed-form spherical intersection and spherical interpolation methods described Smith & Abel (1978).
//' `cpp_tdoa_J` returns the Jacobian of the function with respect arrival times and `cpp_tdoa_error_sens` uses this Jacobian to calculate the length of the axis of greatest positioning error.
//' See details.
//'
//' @param toa Matrix of time of arrival values to position.  Columns are hydrophones.
//' @param pos_hydro Matrix of hydrophone positions.
//' Columns are spatial dimensions.
//' @param c Speed of propagation in medium.
//' @param  W A vector of weights.
//'
//'@details `NA` values will be automatically ignored in the toa matrix and the remaining row values will be used to generate a position.
//' toa values which result in ambiguous positions (two possible position solutions) will be returned as NA values.
//'
//' `cpp_tdoa_error_sens` uses the following algorithm:
//'
//'  1. For each set of arrival times, the TDOA Jacobian is calculated.
//'  2. An identity covariance matrix is transformed by this Jacobian.
//'  3. The resulting transformed covariance matrix is singular value decomposed.
//'  4. The square root of the largest singular value is stored as the error sensitivity for this set of arrival times.
//'
//' By multiplying the resulting values by a standard error of expected time-of-arrival residuals, the error values will then represent the standard deviation of the positioning error along the longest axis of error.
//' This is a useful measure of the approximate maximum telemetry error to expect for a given set of TDOA values.
//'
//' Smith, J. and Abel, J. (1987). Closed-form least-squares source location estimation from range-difference measurements. Ieee Transactions Acoust Speech Signal Process 35, 1661–1669.
//'
//[[Rcpp::export]]
Eigen::MatrixXd cpp_tdoa(
  const Eigen::MatrixXd &toa,
  const Eigen::MatrixXd &pos_hydro,
  const double &c,
  const Eigen::VectorXd &W){

  Eigen::MatrixXd ret;

  ret = TDOA::smith_abel(toa,pos_hydro,c,W);

  return ret;
}

//' @rdname tdoa_methods
//[[Rcpp::export]]
Eigen::MatrixXd cpp_tdoa_J(
    const Eigen::VectorXd &toa,
    const Eigen::MatrixXd &pos_hydro,
    const double &c){

  Eigen::MatrixXd ret;

  ret = TDOA::smith_abel_J(toa,pos_hydro,c);

  return ret;
}

//' @rdname tdoa_methods
//[[Rcpp::export]]
Eigen::VectorXd cpp_tdoa_error_sens(
    const Eigen::MatrixXd &toa,
    const Eigen::MatrixXd &pos_hydro,
    const double &c){

  int n = toa.rows();
  int dims = pos_hydro.cols();

  Eigen::VectorXd ret = Eigen::VectorXd(n);

  Eigen::MatrixXd H;
  Eigen::MatrixXd I = Eigen::MatrixXd::Identity(dims, dims);
  Eigen::MatrixXd Sigma;
  Eigen::VectorXd singular_values;

  Eigen::JacobiSVD<MatrixXd> svd;

  for(int i = 0; i < n; i++){

    // Get jacobian
    H = TDOA::smith_abel_J((VectorXd) toa.row(i),pos_hydro,c);
    // Get position covariance: assume identity covariance matrix
    Sigma = H * H.transpose();
    // singular valued decomposition
    svd = Eigen::JacobiSVD<MatrixXd>(Sigma, Eigen::ComputeThinU | Eigen::ComputeThinV);
    // extract singular values
    singular_values = svd.singularValues();
    // store max, convert to Standard deviation
    ret(i) = pow(singular_values.maxCoeff(), 0.5);
  }

  return ret;
}

//' @rdname tdoa_methods
//[[Rcpp::export]]
Eigen::MatrixXd cpp_tdoa_singular_values(
    const Eigen::MatrixXd &toa,
    const Eigen::MatrixXd &pos_hydro,
    const double &c){

  int n = toa.rows();
  int dims = pos_hydro.cols();

  Eigen::MatrixXd ret = Eigen::MatrixXd(n,dims);

  Eigen::MatrixXd H;
  Eigen::MatrixXd I = Eigen::MatrixXd::Identity(dims, dims);
  Eigen::MatrixXd Sigma;
  Eigen::VectorXd singular_values;

  Eigen::JacobiSVD<MatrixXd> svd;

  for(int i = 0; i < n; i++){

    // Get jacobian
    H = TDOA::smith_abel_J((VectorXd) toa.row(i),pos_hydro,c);
    // Get position covariance: assume identity covariance matrix
    Sigma = H * H.transpose();
    // singular valued decomposition
    svd = Eigen::JacobiSVD<MatrixXd>(Sigma, Eigen::ComputeThinU | Eigen::ComputeThinV);
    // extract singular values
    singular_values = svd.singularValues();

    // store singular values
    for(int j = 0; j < dims; j++){
      ret(i, j) = singular_values(j);
    };
  }

  return ret;
}

// Eigen::VectorXd cpp_linear_interpolation(Eigen::VectorXd x_in, Eigen::VectorXd y_in, Eigen::VectorXd x_out){
//
//   Eigen::VectorXd y_out;
//
//   y_out = (Eigen::VectorXd) interp_linear(
//     (vector<double>) x_in,
//     (vector<double>) y_in,
//     (vector<double>) x_out);
//
//   return(y_out);
//
// }


